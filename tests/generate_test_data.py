import subprocess
import os
from numpy.random import uniform as r
import numpy as np
import pandas as pd


def main():
    create_sm_points(100, "../tests/references/sm.tsv")
    create_2hdm_points(100, "../tests/references/r2hdm.tsv")

    create_cxsm_dark_points(100, "../tests/references/cxsmDark.tsv")
    create_cxsm_broken_points(100, "../tests/references/cxsmBroken.tsv")
    create_rxsm_dark_points(100, "../tests/references/rxsmDark.tsv")
    create_rxsm_broken_points(100, "../tests/references/rxsmBroken.tsv")

    create_n2hdm_broken_points(100, "../tests/references/n2hdmBroken.tsv")
    create_n2hdm_darks_points(100, "../tests/references/n2hdmDarkSinglet.tsv")
    create_n2hdm_darkd_points(100, "../tests/references/n2hdmDarkDoublet.tsv")
    create_n2hdm_darksd_points(100, "../tests/references/n2hdmDarkSingletDoublet.tsv")

    create_c2hdm_points(100, "../tests/references/c2hdm.tsv")


def create_2hdm_points(npoints, filename):
    order = ["type", "tbeta", "m12sq", "mA", "mHp", "mH2", "mH1", "alpha"] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_ZA",
        "b_H1_WpHm",
        "b_H1_AA",
        "b_H1_HpHm",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_ZA",
        "b_H2_WpHm",
        "b_H2_H1H1",
        "b_H2_AA",
        "b_H2_HpHm",
        "w_A",
        "b_A_bb",
        "b_A_tautau",
        "b_A_mumu",
        "b_A_ss",
        "b_A_cc",
        "b_A_tt",
        "b_A_gg",
        "b_A_gamgam",
        "b_A_Zgam",
        "b_A_ZH1",
        "b_A_ZH2",
        "b_A_WpHm",
        "w_Hp",
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WA",
        "w_t",
        "b_t_Wb",
        "b_t_Hpb",
    ]
    dfin = random_2hdm(npoints)
    df = dfin.join(dfin.apply(run_hdecay_2hdm, axis=1))
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_sm_points(npoints, filename):
    order = ["mH"] + [
        "w_H",
        "b_H_bb",
        "b_H_tautau",
        "b_H_mumu",
        "b_H_ss",
        "b_H_cc",
        "b_H_tt",
        "b_H_gg",
        "b_H_gamgam",
        "b_H_Zgam",
        "b_H_WW",
        "b_H_ZZ",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_sm(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_hdecay_sm, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_c2hdm_points(npoints, filename):
    order = ["type", "tbeta", "m12sq", "mHp", "mH1", "mH2", "a1", "a2", "a3"] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_WpHm",
        "b_H1_HpHm",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_ZH1",
        "b_H2_WpHm",
        "b_H2_H1H1",
        "b_H2_HpHm",
        "w_H3",
        "b_H3_bb",
        "b_H3_tautau",
        "b_H3_mumu",
        "b_H3_ss",
        "b_H3_cc",
        "b_H3_tt",
        "b_H3_gg",
        "b_H3_gamgam",
        "b_H3_Zgam",
        "b_H3_WW",
        "b_H3_ZZ",
        "b_H3_ZH1",
        "b_H3_ZH2",
        "b_H3_WpHm",
        "b_H3_H1H1",
        "b_H3_H2H2",
        "b_H3_H1H2",
        "b_H3_HpHm",
        "w_Hp",
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WH3",
        "w_t",
        "b_t_Wb",
        "b_t_Hpb",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_c2hdm(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_chdecay_c2hdm, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_n2hdm_broken_points(npoints, filename):
    order = [
        "type",
        "tbeta",
        "m12sq",
        "mA",
        "mHp",
        "mH1",
        "mH2",
        "mH3",
        "a1",
        "a2",
        "a3",
        "vs",
    ] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_ZA",
        "b_H1_WpHm",
        "b_H1_AA",
        "b_H1_HpHm",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_ZA",
        "b_H2_WpHm",
        "b_H2_H1H1",
        "b_H2_AA",
        "b_H2_HpHm",
        "w_H3",
        "b_H3_bb",
        "b_H3_tautau",
        "b_H3_mumu",
        "b_H3_ss",
        "b_H3_cc",
        "b_H3_tt",
        "b_H3_gg",
        "b_H3_gamgam",
        "b_H3_Zgam",
        "b_H3_WW",
        "b_H3_ZZ",
        "b_H3_ZA",
        "b_H3_WpHm",
        "b_H3_H1H1",
        "b_H3_H1H2",
        "b_H3_H2H2",
        "b_H3_AA",
        "b_H3_HpHm",
        "w_A",
        "b_A_bb",
        "b_A_tautau",
        "b_A_mumu",
        "b_A_ss",
        "b_A_cc",
        "b_A_tt",
        "b_A_gg",
        "b_A_gamgam",
        "b_A_Zgam",
        "b_A_ZH1",
        "b_A_ZH2",
        "b_A_ZH3",
        "b_A_WpHm",
        "w_Hp",
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WH3",
        "b_Hp_WA",
        "w_t",
        "b_t_Wb",
        "b_t_Hpb",
    ]
    dfin = random_n2hdm_broken(npoints)
    df = dfin.join(dfin.apply(run_n2hdecay_broken, axis=1))
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_n2hdm_darks_points(npoints, filename):
    order = [
        "type",
        "tbeta",
        "m12sq",
        "mA",
        "mHp",
        "mHD",
        "mH1",
        "mH2",
        "alpha",
        "L7",
        "L8",
    ] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_ZA",
        "b_H1_WpHm",
        "b_H1_HDHD",
        "b_H1_AA",
        "b_H1_HpHm",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_ZA",
        "b_H2_WpHm",
        "b_H2_HDHD",
        "b_H2_H1H1",
        "b_H2_AA",
        "b_H2_HpHm",
        "w_A",
        "b_A_bb",
        "b_A_tautau",
        "b_A_mumu",
        "b_A_ss",
        "b_A_cc",
        "b_A_tt",
        "b_A_gg",
        "b_A_gamgam",
        "b_A_Zgam",
        "b_A_ZH1",
        "b_A_ZH2",
        "b_A_WpHm",
        "w_Hp",
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WA",
        "w_t",
        "b_t_Wb",
        "b_t_Hpb",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_n2hdm_darks(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_n2hdecay_darks, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_n2hdm_darkd_points(npoints, filename):
    order = ["mA", "mHp", "mHD", "mH1", "mH2", "alpha", "L8", "m22sq", "vs"] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_HDHD",
        "b_H1_ADAD",
        "b_H1_HDpHDm",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_HDHD",
        "b_H2_H1H1",
        "b_H2_ADAD",
        "b_H2_HDpHDm",
        "w_HD",
        "b_HD_ZAD",
        "b_HD_WpHDm",
        "w_AD",
        "b_AD_ZHD",
        "b_AD_WpHDm",
        "w_HDc",
        "b_HDc_WHD",
        "b_HDc_WAD",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_n2hdm_darkd(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_n2hdecay_darkd, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_n2hdm_darksd_points(npoints, filename):
    order = ["mA", "mHp", "mHDD", "mHsm", "mHDS", "m22sq", "mssq"] + [
        "w_Hsm",
        "b_Hsm_bb",
        "b_Hsm_tautau",
        "b_Hsm_mumu",
        "b_Hsm_ss",
        "b_Hsm_cc",
        "b_Hsm_tt",
        "b_Hsm_gg",
        "b_Hsm_gamgam",
        "b_Hsm_Zgam",
        "b_Hsm_WW",
        "b_Hsm_ZZ",
        "b_Hsm_HDDHDD",
        "b_Hsm_ADAD",
        "b_Hsm_HDpHDm",
        "b_Hsm_HDSHDS",
        "w_HDD",
        "b_HDD_ZAD",
        "b_HDD_WpHDm",
        "w_AD",
        "b_AD_ZHDD",
        "b_AD_WpHDm",
        "w_HDc",
        "b_HDc_WHDD",
        "b_HDc_WAD",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_n2hdm_darksd(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_n2hdecay_darksd, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_rxsm_broken_points(npoints, filename):
    order = ["alph1", "mH1", "mH2", "vs"] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_H2H2",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_H1H1",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_rxsm_b(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_shdecay_rb, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_rxsm_dark_points(npoints, filename):
    order = ["mH1", "mD", "m2s"] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_HDHD",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_rxsm_d(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_shdecay_rd, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_cxsm_broken_points(npoints, filename):
    order = ["alph1", "alph2", "alph3", "m1", "m3", "vs"] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_H2H2",
        "b_H1_H2H3",
        "b_H1_H3H3",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_H1H1",
        "b_H2_H1H3",
        "b_H2_H3H3",
        "w_H3",
        "b_H3_bb",
        "b_H3_tautau",
        "b_H3_mumu",
        "b_H3_ss",
        "b_H3_cc",
        "b_H3_tt",
        "b_H3_gg",
        "b_H3_gamgam",
        "b_H3_Zgam",
        "b_H3_WW",
        "b_H3_ZZ",
        "b_H3_H1H1",
        "b_H3_H1H2",
        "b_H3_H2H2",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_cxsm_b(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_shdecay_cb, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def create_cxsm_dark_points(npoints, filename):
    order = ["alph1", "m1", "m2", "m3", "vs", "a1"] + [
        "w_H1",
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_H2H2",
        "b_H1_H2HD",
        "b_H1_HDHD",
        "w_H2",
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_H1H1",
        "b_H2_H1HD",
        "b_H2_HDHD",
    ]
    df = pd.DataFrame()
    while len(df) < npoints:
        dfin = random_cxsm_d(npoints - len(df))
        df = pd.concat(
            [df, dfin.join(dfin.apply(run_shdecay_cd, axis=1)).dropna()]
        ).reset_index(drop=True)
    df.loc[:, order].to_csv(filename, sep="\t", header=False)


def run_hdecay_2hdm(row, path="_deps/hdecay-src"):
    with cd(path):
        write_hdecayin_2hdm(row, "hdecay.in")
        subprocess.run(os.path.join("./run"))
        return read_hdecay_2hdm("./")


def run_hdecay_sm(row, path="_deps/hdecay-src"):
    with cd(path):
        write_hdecayin_sm(row, "hdecay.in")
        subprocess.run(os.path.join("./run"))
        return read_hdecay_sm("./")


def run_chdecay_c2hdm(row, path="_deps/c2hdm_hdecay-src"):
    with cd(path):
        write_chdecayin_c2hdm(row, "hdecay.in")
        subprocess.run(os.path.join("./run"))
        return read_chdecay_c2hdm("./")


def run_n2hdecay_broken(row, path="_deps/n2hdecay-build/"):
    with cd(path):
        write_n2hdecayin_broken(row, "n2hdecay.in")
        subprocess.run(os.path.join("./n2hdecay"))
        return read_n2hdecay_broken("./")


def run_n2hdecay_darks(row, path="_deps/n2hdecay-build/"):
    with cd(path):
        write_n2hdecayin_darks(row, "n2hdecay.in")
        subprocess.run(os.path.join("./n2hdecay"))
        return read_n2hdecay_darks("./")


def run_n2hdecay_darkd(row, path="_deps/n2hdecay-build/"):
    with cd(path):
        write_n2hdecayin_darkd(row, "n2hdecay.in")
        subprocess.run(os.path.join("./n2hdecay"))
        return read_n2hdecay_darkd("./")


def run_n2hdecay_darksd(row, path="_deps/n2hdecay-build/"):
    with cd(path):
        write_n2hdecayin_darksd(row, "n2hdecay.in")
        subprocess.run(os.path.join("./n2hdecay"))
        return read_n2hdecay_darksd("./")


def run_shdecay_rb(row, path="_deps/shdecay-src/"):
    with cd(path):
        write_shdecayin_rb(row, "shdecay.in")
        subprocess.run(os.path.join("./run"))
        return read_shdecay_rb("./")


def run_shdecay_rd(row, path="_deps/shdecay-src/"):
    with cd(path):
        write_shdecayin_rd(row, "shdecay.in")
        subprocess.run(os.path.join("./run"))
        return read_shdecay_rd("./")


def run_shdecay_cb(row, path="_deps/shdecay-src/"):
    with cd(path):
        write_shdecayin_cb(row, "shdecay.in")
        subprocess.run(os.path.join("./run"))
        return read_shdecay_cb("./")


def run_shdecay_cd(row, path="_deps/shdecay-src/"):
    with cd(path):
        write_shdecayin_cd(row, "shdecay.in")
        subprocess.run(os.path.join("./run"))
        return read_shdecay_cd("./")


class cd:
    """Context manager for changing the current working directory"""

    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def default_const():
    return [
        0.118e0,
        0.095e0,
        0.986e0,
        4.180e0,
        172.5e0,
        1.77682e0,
        0.1056583715e0,
        137.0359997e0,
        1.1663787e-5,
        2.08430e0,
        2.49427e0,
        91.15348e0,
        80.35797e0,
        0.9991e0,
        0.0404e0,
        0.00867e0,
        0.0412e0,
        0.97344e0,
        0.22520e0,
        0.00351e0,
        0.22534e0,
        0.97427e0,
    ]


def write_n2hdecayin_darks(row, outfile="n2hdecay_test.in"):
    lines = [
        "*********************** N2HDM Input ***********************",
        "** TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped) **",
        "** PHASE: 1 (broken), 2 (dark singlet), 3 (dark doublet) **",
        "**************** see xxxx.yyyy for details ****************",
        "PHASE    = 2",
        "TYPE     = " + str(int(row["type"])),
        "***********************************************************",
        "tbeta   = " + str(row["tbeta"]),
        "m12sq   = " + str(row["m12sq"]),
        "m_A     = " + str(row["mA"]),
        "m_Hc    = " + str(row["mHp"]),
        "vs      = 0.",
        "********************** PHASE = 1, broken phase ************",
        "m_H1    = 1.",
        "m_H2    = 1.",
        "m_H3    = 1.",
        "a1      = 1.",
        "a2      = 1.",
        "a3      = 1.",
        "********************* PHASE = 2, dark singlet **************",
        "m_H1     = " + str(row["mH1"]),
        "m_H2     = " + str(row["mH2"]),
        "m_HD    = " + str(row["mHD"]),
        "alpha   = " + str(row["alpha"]),
        "L7      = " + str(row["L7"]),
        "L8      = " + str(row["L8"]),
        "********** (L6 is irrelevant for the decays)  *************",
        "************* PHASE = 3, dark doublet phase ***************",
        "m_H1    = 8.17422761D2",
        "m_H2    = 1.25090000D2",
        "m_HD    = 5.0D3",
        "alpha   = 1.46729273",
        "L8      = 0.5",
        "m22sq    = 1.25090000D2",
        "********** (L1 is irrelevant for the decays)  *************",
        "********* PHASE = 4, dark singlet & doublet phase *********",
        "m_Hsm   = 1.",
        "m_HDD   = 1.",
        "m_HDS   = 1.",
        "m22sq   = 1.",
        "mssq    = 1.",
        "L2, L6 and L8 are irrelevant for the decays",
        "***********************************************************",
        "************* SM Input, hdecay default values *************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "***********************************************************",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_n2hdecayin_darkd(row, outfile="n2hdecay_test.in"):
    lines = [
        "*********************** N2HDM Input ***********************",
        "** TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped) **",
        "** PHASE: 1 (broken), 2 (dark singlet), 3 (dark doublet) **",
        "**************** see xxxx.yyyy for details ****************",
        "PHASE    = 3",
        "TYPE     = 1",
        "***********************************************************",
        "TanBeta = 1.",
        "M_12^2  = 1.",
        "m_A     = " + str(row["mA"]),
        "m_H+-   = " + str(row["mHp"]),
        "v_S     = " + str(row["vs"]),
        "********************** PHASE = 1, broken phase ************",
        "m_H1    = 1.",
        "m_H2    = 1.",
        "m_H3    = 1.",
        "alpha1  = 1.",
        "alpha2  = 1.",
        "alpha3  = 1.",
        "********************** PHASE = 2, dark phase **************",
        "m_H     = 1.",
        "m_h     = 1.",
        "m_DM    = 1.",
        "alpha   = 1.",
        "L7      = 1.",
        "L8      = 1.",
        "********** (L6 is irrelevant for the decays)  *************",
        "************* PHASE = 3, dark doublet phase ***************",
        "m_H1    = " + str(row["mH1"]),
        "m_H2    = " + str(row["mH2"]),
        "m_DM    = " + str(row["mHD"]),
        "alpha   = " + str(row["alpha"]),
        "L8      = " + str(row["L8"]),
        "m22sq   = " + str(row["m22sq"]),
        "********** (L1 is irrelevant for the decays)  *************",
        "********* PHASE = 4, dark singlet & doublet phase *********",
        "m_Hsm   = 1.",
        "m_HDD   = 1.",
        "m_HDS   = 1.",
        "m22sq   = 1.",
        "mssq    = 1.",
        "L2, L6 and L8 are irrelevant for the decays",
        "***********************************************************",
        "************* SM Input, hdecay default values *************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "***********************************************************",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_n2hdecayin_darksd(row, outfile="n2hdecay_test.in"):
    lines = [
        "*********************** N2HDM Input ***********************",
        "** TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped) **",
        "** PHASE: 1 (broken), 2 (dark singlet), 3 (dark doublet) **",
        "**************** see xxxx.yyyy for details ****************",
        "PHASE    = 4",
        "TYPE     = 1",
        "***********************************************************",
        "TanBeta = 1.",
        "M_12^2  = 1.",
        "m_A     = " + str(row["mA"]),
        "m_H+-   = " + str(row["mHp"]),
        "v_S     = 0.",
        "********************** PHASE = 1, broken phase ************",
        "m_H1    = 1.",
        "m_H2    = 1.",
        "m_H3    = 1.",
        "alpha1  = 1.",
        "alpha2  = 1.",
        "alpha3  = 1.",
        "********************** PHASE = 2, dark phase **************",
        "m_H     = 1.",
        "m_h     = 1.",
        "m_DM    = 1.",
        "alpha   = 1.",
        "L7      = 1.",
        "L8      = 1.",
        "********** (L6 is irrelevant for the decays)  *************",
        "************* PHASE = 3, dark doublet phase ***************",
        "m_H1    = 1.",
        "m_H2    = 1.",
        "m_HD    = 1.",
        "alpha   = 1.",
        "L8      = 1.",
        "m22sq   = 1.",
        "********** (L1 is irrelevant for the decays)  *************",
        "********* PHASE = 4, dark singlet & doublet phase *********",
        "m_Hsm   = {}".format(row["mHsm"]),
        "m_HDD   = {}".format(row["mHDD"]),
        "m_HDS   = {}".format(row["mHDS"]),
        "m22sq   = {}".format(row["m22sq"]),
        "mssq    = {}".format(row["mssq"]),
        "L2, L6 and L8 are irrelevant for the decays",
        "***********************************************************",
        "************* SM Input, hdecay default values *************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "***********************************************************",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_n2hdecayin_broken(row, outfile="n2hdecay_test.in"):
    lines = [
        "*********************** N2HDM Input ***********************",
        "** TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped) **",
        "** PHASE: 1 (broken), 2 (dark singlet), 3 (dark doublet) **",
        "**************** see xxxx.yyyy for details ****************",
        "PHASE    = 1",
        "YUKTYPE  = " + str(int(row["type"])),
        "***********************************************************",
        "TanBeta = " + str(row["tbeta"]),
        "M_12^2  = " + str(row["m12sq"]),
        "m_A     = " + str(row["mA"]),
        "m_H+-   = " + str(row["mHp"]),
        "v_S     = " + str(row["vs"]),
        "********************** PHASE = 1, broken phase ************",
        "m_H1    = " + str(row["mH1"]),
        "m_H2    = " + str(row["mH2"]),
        "m_H3    = " + str(row["mH3"]),
        "alpha1  = " + str(row["a1"]),
        "alpha2  = " + str(row["a2"]),
        "alpha3  = " + str(row["a3"]),
        "******************** PHASE = 2, dark singlet **************",
        "m_H     = 1.",
        "m_h     = 1.",
        "m_DM    = 1.",
        "alpha   = 1.",
        "L7      = 1.",
        "L8      = 1.",
        "************* PHASE = 3, dark doublet phase ***************",
        "****** PHASE = 3, dark doublet phase ***********",
        "m_H1    = 1.25090000D2",
        "m_H2    = 8.17422761D2",
        "m_DM    = 5.0D3",
        "alpha   = 1.46729273",
        "L8      = 0.5",
        "m22sq   = 1.25090000D2",
        "********** (L1 is irrelevant for the decays)  *************",
        "********* PHASE = 4, dark singlet & doublet phase *********",
        "m_Hsm   = 1.",
        "m_HDD   = 1.",
        "m_HDS   = 1.",
        "m22sq   = 1.",
        "mssq    = 1.",
        "L2, L6 and L8 are irrelevant for the decays",
        "***********************************************************",
        "************* SM Input, hdecay default values *************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "***********************************************************",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_hdecayin_2hdm(row, outfile="hdecay_test.in"):
    lines = [
        "SLHAIN   = 0",
        "SLHAOUT  = 0",
        "COUPVAR  = 0",
        "HIGGS    = 5",
        "OMIT ELW = 0",
        "SM4      = 0",
        "FERMPHOB = 0",
        "2HDM     = 1",
        "MODEL    = 1",
        "TGBET    = 30.D0",
        "MABEG    = 125.D0",
        "MAEND    = 1000.D0",
        "NMA      = 1",
        "********************* hMSSM (MODEL = 10) *********************************",
        "MHL      = 125.D0",
        "**************************************************************************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "********************* 4TH GENERATION *************************************",
        "  SCENARIO FOR ELW. CORRECTIONS TO H -> GG (EVERYTHING IN GEV):",
        "  GG_ELW = 1: MTP = 500    MBP = 450    MNUP = 375    MEP = 450",
        "  GG_ELW = 2: MBP = MNUP = MEP = 600    MTP = MBP+50*(1+LOG(M_H/115)/5)",
        "",
        "GG_ELW   = 1",
        "MTP      = 500.D0",
        "MBP      = 450.D0",
        "MNUP     = 375.D0",
        "MEP      = 450.D0",
        "************************** 2 Higgs Doublet Model *************************",
        "  TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped)",
        "  PARAM: 1 (masses), 2 (lambda_i)",
        "",
        "PARAM    = 1",
        "TYPE     = " + str(int(row["type"])),
        "********************",
        "TGBET2HDM= " + str(row["tbeta"]),
        "M_12^2   = " + str(row["m12sq"]),
        "******************** PARAM=1:",
        "ALPHA_H  = " + str(row["alpha"]),
        "MHL      = " + str(row["mH1"]),
        "MHH      = " + str(row["mH2"]),
        "MHA      = " + str(row["mA"]),
        "MH+-     = " + str(row["mHp"]),
        "******************** PARAM=2:",
        "LAMBDA1  = 2.6885665050462264D0",
        "LAMBDA2  = 0.000156876030254505681D0",
        "LAMBDA3  = 0.46295674052962260D0",
        "LAMBDA4  = 0.96605498373771792D0",
        "LAMBDA5  = -0.88138084173680198D0",
        "**************************************************************************",
        "SUSYSCALE= 1000.D0",
        "MU       = 1000.D0",
        "M2       = 1000.D0",
        "MGLUINO  = 1000.D0",
        "MSL1     = 1000.D0",
        "MER1     = 1000.D0",
        "MQL1     = 1000.D0",
        "MUR1     = 1000.D0",
        "MDR1     = 1000.D0",
        "MSL      = 1000.D0",
        "MER      = 1000.D0",
        "MSQ      = 1000.D0",
        "MUR      = 1000.D0",
        "MDR      = 1000.D0",
        "AL       = 2450.D0",
        "AU       = 2450.D0",
        "AD       = 2450.D0",
        "ON-SHELL = 0",
        "ON-SH-WZ = 0",
        "IPOLE    = 0",
        "OFF-SUSY = 0",
        "INDIDEC  = 0",
        "NF-GG    = 5",
        "IGOLD    = 0",
        "MPLANCK  = 2.4D18",
        "MGOLD    = 1.D-13",
        "******************* VARIATION OF HIGGS COUPLINGS *************************",
        "ELWK     = 0",
        "CW       = 1.D0",
        "CZ       = 1.D0",
        "Ctau     = 1.D0",
        "Cmu      = 1.D0",
        "Ct       = 1.D0",
        "Cb       = 1.D0",
        "Cc       = 1.D0",
        "Cs       = 1.D0",
        "Cgaga    = 0.D0",
        "Cgg      = 0.D0",
        "CZga     = 0.D0",
        "********************* 4TH GENERATION *************************************",
        "Ctp      = 0.D0",
        "Cbp      = 0.D0",
        "Cnup     = 0.D0",
        "Cep      = 0.D0",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_hdecayin_sm(row, outfile="hdecay_test.in"):
    lines = [
        "SLHAIN   = 0",
        "SLHAOUT  = 0",
        "COUPVAR  = 0",
        "HIGGS    = 0",
        "OMIT ELW = 1",
        "SM4      = 0",
        "FERMPHOB = 0",
        "2HDM     = 0",
        "MODEL    = 1",
        "TGBET    = 30.D0",
        "MABEG    = " + str(row["mH"]),
        "MAEND    = " + str(row["mH"]),
        "NMA      = 1",
        "********************* hMSSM (MODEL = 10) *********************************",
        "MHL      = 125.D0",
        "**************************************************************************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "********************* 4TH GENERATION *************************************",
        "  SCENARIO FOR ELW. CORRECTIONS TO H -> GG (EVERYTHING IN GEV):",
        "  GG_ELW = 1: MTP = 500    MBP = 450    MNUP = 375    MEP = 450",
        "  GG_ELW = 2: MBP = MNUP = MEP = 600    MTP = MBP+50*(1+LOG(M_H/115)/5)",
        "",
        "GG_ELW   = 1",
        "MTP      = 500.D0",
        "MBP      = 450.D0",
        "MNUP     = 375.D0",
        "MEP      = 450.D0",
        "************************** 2 Higgs Doublet Model *************************",
        "  TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped)",
        "  PARAM: 1 (masses), 2 (lambda_i)",
        "",
        "PARAM    = 1",
        "TYPE     = 1",
        "********************",
        "TGBET2HDM= 1.",
        "M_12^2   = 1.",
        "******************** PARAM=1:",
        "ALPHA_H  = 1.",
        "MHL      = 1.",
        "MHH      = 1.",
        "MHA      = 1.",
        "MH+-     = 1.",
        "******************** PARAM=2:",
        "LAMBDA1  = 2.6885665050462264D0",
        "LAMBDA2  = 0.000156876030254505681D0",
        "LAMBDA3  = 0.46295674052962260D0",
        "LAMBDA4  = 0.96605498373771792D0",
        "LAMBDA5  = -0.88138084173680198D0",
        "**************************************************************************",
        "SUSYSCALE= 1000.D0",
        "MU       = 1000.D0",
        "M2       = 1000.D0",
        "MGLUINO  = 1000.D0",
        "MSL1     = 1000.D0",
        "MER1     = 1000.D0",
        "MQL1     = 1000.D0",
        "MUR1     = 1000.D0",
        "MDR1     = 1000.D0",
        "MSL      = 1000.D0",
        "MER      = 1000.D0",
        "MSQ      = 1000.D0",
        "MUR      = 1000.D0",
        "MDR      = 1000.D0",
        "AL       = 2450.D0",
        "AU       = 2450.D0",
        "AD       = 2450.D0",
        "ON-SHELL = 0",
        "ON-SH-WZ = 0",
        "IPOLE    = 0",
        "OFF-SUSY = 0",
        "INDIDEC  = 0",
        "NF-GG    = 5",
        "IGOLD    = 0",
        "MPLANCK  = 2.4D18",
        "MGOLD    = 1.D-13",
        "******************* VARIATION OF HIGGS COUPLINGS *************************",
        "ELWK     = 0",
        "CW       = 1.D0",
        "CZ       = 1.D0",
        "Ctau     = 1.D0",
        "Cmu      = 1.D0",
        "Ct       = 1.D0",
        "Cb       = 1.D0",
        "Cc       = 1.D0",
        "Cs       = 1.D0",
        "Cgaga    = 0.D0",
        "Cgg      = 0.D0",
        "CZga     = 0.D0",
        "********************* 4TH GENERATION *************************************",
        "Ctp      = 0.D0",
        "Cbp      = 0.D0",
        "Cnup     = 0.D0",
        "Cep      = 0.D0",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_chdecayin_c2hdm(row, outfile="hdecay_test.in"):
    lines = [
        "SLHAIN   = 0",
        "SLHAOUT  = 0",
        "COUPVAR  = 0",
        "HIGGS    = 5",
        "OMIT ELW = 0",
        "SM4      = 0",
        "FERMPHOB = 0",
        "2HDM     = 1",
        "C2HDM    = 1",
        "MODEL    = 1",
        "TGBET    = 30.D0",
        "MABEG    = 125.D0",
        "MAEND    = 1000.D0",
        "NMA      = 1",
        "********************* hMSSM (MODEL = 10) *********************************",
        "MHL      = 125.D0",
        "**************************************************************************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "********************* 4TH GENERATION *************************************",
        "  SCENARIO FOR ELW. CORRECTIONS TO H -> GG (EVERYTHING IN GEV):",
        "  GG_ELW = 1: MTP = 500    MBP = 450    MNUP = 375    MEP = 450",
        "  GG_ELW = 2: MBP = MNUP = MEP = 600    MTP = MBP+50*(1+LOG(M_H/115)/5)",
        "",
        "GG_ELW   = 1",
        "MTP      = 500.D0",
        "MBP      = 450.D0",
        "MNUP     = 375.D0",
        "MEP      = 450.D0",
        "************************** 2 Higgs Doublet Model *************************",
        "  TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped)",
        "  PARAM: 1 (masses), 2 (lambda_i)",
        "",
        "PARAM    = 1",
        "TYPE     = 1",
        "********************",
        "TGBET2HDM= 1.",
        "M_12^2   = 1.",
        "******************** PARAM=1:",
        "ALPHA_H  = 1.",
        "MHL      = 1.",
        "MHH      = 1.",
        "MHA      = 1.",
        "MH+-     = 1.",
        "******************** PARAM=2:",
        "LAMBDA1  = 2.6885665050462264D0",
        "LAMBDA2  = 0.000156876030254505681D0",
        "LAMBDA3  = 0.46295674052962260D0",
        "LAMBDA4  = 0.96605498373771792D0",
        "LAMBDA5  = -0.88138084173680198D0",
        "*********************** complex 2 Higgs Doublet Model ********************",
        "M1_2HDM  = " + str(row["mH1"]),
        "M2_2HDM  = " + str(row["mH2"]),
        "MCH_2HDM = " + str(row["mHp"]),
        "alp1_2HDM= " + str(row["a1"]),
        "alp2_2HDM= " + str(row["a2"]),
        "alp3_2HDM= " + str(row["a3"]),
        "tbetc2HDM= " + str(row["tbeta"]),
        "R_M12_H2 = " + str(row["m12sq"]),
        "TYPE_cp  = " + str(int(row["type"])),
        "**************************************************************************",
        "SUSYSCALE= 1000.D0",
        "MU       = 1000.D0",
        "M2       = 1000.D0",
        "MGLUINO  = 1000.D0",
        "MSL1     = 1000.D0",
        "MER1     = 1000.D0",
        "MQL1     = 1000.D0",
        "MUR1     = 1000.D0",
        "MDR1     = 1000.D0",
        "MSL      = 1000.D0",
        "MER      = 1000.D0",
        "MSQ      = 1000.D0",
        "MUR      = 1000.D0",
        "MDR      = 1000.D0",
        "AL       = 2450.D0",
        "AU       = 2450.D0",
        "AD       = 2450.D0",
        "ON-SHELL = 0",
        "ON-SH-WZ = 0",
        "IPOLE    = 0",
        "OFF-SUSY = 0",
        "INDIDEC  = 0",
        "NF-GG    = 5",
        "IGOLD    = 0",
        "MPLANCK  = 2.4D18",
        "MGOLD    = 1.D-13",
        "******************* VARIATION OF HIGGS COUPLINGS *************************",
        "ELWK     = 0",
        "CW       = 1.D0",
        "CZ       = 1.D0",
        "Ctau     = 1.D0",
        "Cmu      = 1.D0",
        "Ct       = 1.D0",
        "Cb       = 1.D0",
        "Cc       = 1.D0",
        "Cs       = 1.D0",
        "Cgaga    = 0.D0",
        "Cgg      = 0.D0",
        "CZga     = 0.D0",
        "********************* 4TH GENERATION *************************************",
        "Ctp      = 0.D0",
        "Cbp      = 0.D0",
        "Cnup     = 0.D0",
        "Cep      = 0.D0",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_shdecayin_rb(row, outfile="hdecay_test.in"):
    lines = [
        "SLHAIN   = 0",
        "SLHAOUT  = 0",
        "COUPVAR  = 0",
        "HIGGS    = 0",
        "OMIT ELW = 1",
        "SM4      = 0",
        "FERMPHOB = 0",
        "2HDM     = 0",
        "MODEL    = 1",
        "TGBET    = 30.D0",
        "MABEG    = 100",
        "MAEND    = 100",
        "NMA      = 1",
        "********************* hMSSM (MODEL = 10) *********************************",
        "MHL      = 125.D0",
        "**************************************************************************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "********************* 4TH GENERATION *************************************",
        "  SCENARIO FOR ELW. CORRECTIONS TO H -> GG (EVERYTHING IN GEV):",
        "  GG_ELW = 1: MTP = 500    MBP = 450    MNUP = 375    MEP = 450",
        "  GG_ELW = 2: MBP = MNUP = MEP = 600    MTP = MBP+50*(1+LOG(M_H/115)/5)",
        "",
        "GG_ELW   = 1",
        "MTP      = 500.D0",
        "MBP      = 450.D0",
        "MNUP     = 375.D0",
        "MEP      = 450.D0",
        "************************** 2 Higgs Doublet Model *************************",
        "  TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped)",
        "  PARAM: 1 (masses), 2 (lambda_i)",
        "",
        "PARAM    = 1",
        "TYPE     = 1",
        "********************",
        "TGBET2HDM= 1.",
        "M_12^2   = 1.",
        "******************** PARAM=1:",
        "ALPHA_H  = 1.",
        "MHL      = 1.",
        "MHH      = 1.",
        "MHA      = 1.",
        "MH+-     = 1.",
        "******************** PARAM=2:",
        "LAMBDA1  = 2.6885665050462264D0",
        "LAMBDA2  = 0.000156876030254505681D0",
        "LAMBDA3  = 0.46295674052962260D0",
        "LAMBDA4  = 0.96605498373771792D0",
        "LAMBDA5  = -0.88138084173680198D0",
        "**************************************************************************",
        "SUSYSCALE= 1000.D0",
        "MU       = 1000.D0",
        "M2       = 1000.D0",
        "MGLUINO  = 1000.D0",
        "MSL1     = 1000.D0",
        "MER1     = 1000.D0",
        "MQL1     = 1000.D0",
        "MUR1     = 1000.D0",
        "MDR1     = 1000.D0",
        "MSL      = 1000.D0",
        "MER      = 1000.D0",
        "MSQ      = 1000.D0",
        "MUR      = 1000.D0",
        "MDR      = 1000.D0",
        "AL       = 2450.D0",
        "AU       = 2450.D0",
        "AD       = 2450.D0",
        "NNLO (M) = 1",
        "ON-SHELL = 0",
        "ON-SH-WZ = 0",
        "IPOLE    = 0",
        "OFF-SUSY = 0",
        "INDIDEC  = 0",
        "NF-GG    = 5",
        "IGOLD    = 0",
        "MPLANCK  = 2.4D18",
        "MGOLD    = 1.D-13",
        "******************* VARIATION OF HIGGS COUPLINGS *************************",
        "ELWK     = 0",
        "CW       = 1.D0",
        "CZ       = 1.D0",
        "Ctau     = 1.D0",
        "Cmu      = 1.D0",
        "Ct       = 1.D0",
        "Cb       = 1.D0",
        "Cc       = 1.D0",
        "Cs       = 1.D0",
        "Cgaga    = 0.D0",
        "Cgg      = 0.D0",
        "CZga     = 0.D0",
        "********************* 4TH GENERATION *************************************",
        "Ctp      = 0.D0",
        "Cbp      = 0.D0",
        "Cnup     = 0.D0",
        "Cep      = 0.D0",
        "********************** real or complex singlet Model *********************",
        "Singlet Extension: 1 - yes, 0 - no (based on SM w/o EW corrections)",
        "Model: 1 - real broken phase, 2 - real dark matter phase",
        "       3 - complex broken phase, 4 - complex dark matter phase",
        "isinglet = 1",
        "icxSM    = 1",
        "*** real singlet broken phase ***",
        "alph1    = " + str(row["alph1"]),
        "mH1      = " + str(row["mH1"]),
        "mH2      = " + str(row["mH2"]),
        "vs       = " + str(row["vs"]),
        "*** real singlet dark matter phase ***",
        "mH1      = 125.1D0",
        "mD       = 48.0215D0",
        "m2s      = -463128.D0",
        "lambdas  = 3.56328D0",
        "*** complex singlet broken phase ***",
        "alph1    = 0.160424D0",
        "alph2    = -0.362128D0",
        "alph3    = -0.552533D0",
        "m1       = 125.518D0",
        "m3       = 500.705D0",
        "vs       = 510.922D0",
        "*** complex singlet dark matter phase ***",
        "alph1    = -0.317120D0",
        "m1       = 125.3D0",
        "m2       = 400.D0",
        "m3       = 731.205D0",
        "vs       = 522.181D0",
        "a1       = -3.12115D07",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_shdecayin_rd(row, outfile="hdecay_test.in"):
    lines = [
        "SLHAIN   = 0",
        "SLHAOUT  = 0",
        "COUPVAR  = 0",
        "HIGGS    = 0",
        "OMIT ELW = 1",
        "SM4      = 0",
        "FERMPHOB = 0",
        "2HDM     = 0",
        "MODEL    = 1",
        "TGBET    = 30.D0",
        "MABEG    = 100",
        "MAEND    = 100",
        "NMA      = 1",
        "********************* hMSSM (MODEL = 10) *********************************",
        "MHL      = 125.D0",
        "**************************************************************************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "********************* 4TH GENERATION *************************************",
        "  SCENARIO FOR ELW. CORRECTIONS TO H -> GG (EVERYTHING IN GEV):",
        "  GG_ELW = 1: MTP = 500    MBP = 450    MNUP = 375    MEP = 450",
        "  GG_ELW = 2: MBP = MNUP = MEP = 600    MTP = MBP+50*(1+LOG(M_H/115)/5)",
        "",
        "GG_ELW   = 1",
        "MTP      = 500.D0",
        "MBP      = 450.D0",
        "MNUP     = 375.D0",
        "MEP      = 450.D0",
        "************************** 2 Higgs Doublet Model *************************",
        "  TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped)",
        "  PARAM: 1 (masses), 2 (lambda_i)",
        "",
        "PARAM    = 1",
        "TYPE     = 1",
        "********************",
        "TGBET2HDM= 1.",
        "M_12^2   = 1.",
        "******************** PARAM=1:",
        "ALPHA_H  = 1.",
        "MHL      = 1.",
        "MHH      = 1.",
        "MHA      = 1.",
        "MH+-     = 1.",
        "******************** PARAM=2:",
        "LAMBDA1  = 2.6885665050462264D0",
        "LAMBDA2  = 0.000156876030254505681D0",
        "LAMBDA3  = 0.46295674052962260D0",
        "LAMBDA4  = 0.96605498373771792D0",
        "LAMBDA5  = -0.88138084173680198D0",
        "**************************************************************************",
        "SUSYSCALE= 1000.D0",
        "MU       = 1000.D0",
        "M2       = 1000.D0",
        "MGLUINO  = 1000.D0",
        "MSL1     = 1000.D0",
        "MER1     = 1000.D0",
        "MQL1     = 1000.D0",
        "MUR1     = 1000.D0",
        "MDR1     = 1000.D0",
        "MSL      = 1000.D0",
        "MER      = 1000.D0",
        "MSQ      = 1000.D0",
        "MUR      = 1000.D0",
        "MDR      = 1000.D0",
        "AL       = 2450.D0",
        "AU       = 2450.D0",
        "AD       = 2450.D0",
        "NNLO (M) = 1",
        "ON-SHELL = 0",
        "ON-SH-WZ = 0",
        "IPOLE    = 0",
        "OFF-SUSY = 0",
        "INDIDEC  = 0",
        "NF-GG    = 5",
        "IGOLD    = 0",
        "MPLANCK  = 2.4D18",
        "MGOLD    = 1.D-13",
        "******************* VARIATION OF HIGGS COUPLINGS *************************",
        "ELWK     = 0",
        "CW       = 1.D0",
        "CZ       = 1.D0",
        "Ctau     = 1.D0",
        "Cmu      = 1.D0",
        "Ct       = 1.D0",
        "Cb       = 1.D0",
        "Cc       = 1.D0",
        "Cs       = 1.D0",
        "Cgaga    = 0.D0",
        "Cgg      = 0.D0",
        "CZga     = 0.D0",
        "********************* 4TH GENERATION *************************************",
        "Ctp      = 0.D0",
        "Cbp      = 0.D0",
        "Cnup     = 0.D0",
        "Cep      = 0.D0",
        "********************** real or complex singlet Model *********************",
        "Singlet Extension: 1 - yes, 0 - no (based on SM w/o EW corrections)",
        "Model: 1 - real broken phase, 2 - real dark matter phase",
        "       3 - complex broken phase, 4 - complex dark matter phase",
        "isinglet = 1",
        "icxSM    = 2",
        "*** real singlet broken phase ***",
        "alph1    = 1.",
        "mH1      = 1.",
        "mH2      = 1.",
        "vs       = 1.",
        "*** real singlet dark matter phase ***",
        "mH1      = " + str(row["mH1"]),
        "mD       = " + str(row["mD"]),
        "m2s      = " + str(row["m2s"]),
        "lambdas  = 3.56328D0",
        "*** complex singlet broken phase ***",
        "alph1    = 0.160424D0",
        "alph2    = -0.362128D0",
        "alph3    = -0.552533D0",
        "m1       = 125.518D0",
        "m3       = 500.705D0",
        "vs       = 510.922D0",
        "*** complex singlet dark matter phase ***",
        "alph1    = -0.317120D0",
        "m1       = 125.3D0",
        "m2       = 400.D0",
        "m3       = 731.205D0",
        "vs       = 522.181D0",
        "a1       = -3.12115D07",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_shdecayin_cb(row, outfile="hdecay_test.in"):
    lines = [
        "SLHAIN   = 0",
        "SLHAOUT  = 0",
        "COUPVAR  = 0",
        "HIGGS    = 0",
        "OMIT ELW = 1",
        "SM4      = 0",
        "FERMPHOB = 0",
        "2HDM     = 0",
        "MODEL    = 1",
        "TGBET    = 30.D0",
        "MABEG    = 100",
        "MAEND    = 100",
        "NMA      = 1",
        "********************* hMSSM (MODEL = 10) *********************************",
        "MHL      = 125.D0",
        "**************************************************************************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "********************* 4TH GENERATION *************************************",
        "  SCENARIO FOR ELW. CORRECTIONS TO H -> GG (EVERYTHING IN GEV):",
        "  GG_ELW = 1: MTP = 500    MBP = 450    MNUP = 375    MEP = 450",
        "  GG_ELW = 2: MBP = MNUP = MEP = 600    MTP = MBP+50*(1+LOG(M_H/115)/5)",
        "",
        "GG_ELW   = 1",
        "MTP      = 500.D0",
        "MBP      = 450.D0",
        "MNUP     = 375.D0",
        "MEP      = 450.D0",
        "************************** 2 Higgs Doublet Model *************************",
        "  TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped)",
        "  PARAM: 1 (masses), 2 (lambda_i)",
        "",
        "PARAM    = 1",
        "TYPE     = 1",
        "********************",
        "TGBET2HDM= 1.",
        "M_12^2   = 1.",
        "******************** PARAM=1:",
        "ALPHA_H  = 1.",
        "MHL      = 1.",
        "MHH      = 1.",
        "MHA      = 1.",
        "MH+-     = 1.",
        "******************** PARAM=2:",
        "LAMBDA1  = 2.6885665050462264D0",
        "LAMBDA2  = 0.000156876030254505681D0",
        "LAMBDA3  = 0.46295674052962260D0",
        "LAMBDA4  = 0.96605498373771792D0",
        "LAMBDA5  = -0.88138084173680198D0",
        "**************************************************************************",
        "SUSYSCALE= 1000.D0",
        "MU       = 1000.D0",
        "M2       = 1000.D0",
        "MGLUINO  = 1000.D0",
        "MSL1     = 1000.D0",
        "MER1     = 1000.D0",
        "MQL1     = 1000.D0",
        "MUR1     = 1000.D0",
        "MDR1     = 1000.D0",
        "MSL      = 1000.D0",
        "MER      = 1000.D0",
        "MSQ      = 1000.D0",
        "MUR      = 1000.D0",
        "MDR      = 1000.D0",
        "AL       = 2450.D0",
        "AU       = 2450.D0",
        "AD       = 2450.D0",
        "NNLO (M) = 1",
        "ON-SHELL = 0",
        "ON-SH-WZ = 0",
        "IPOLE    = 0",
        "OFF-SUSY = 0",
        "INDIDEC  = 0",
        "NF-GG    = 5",
        "IGOLD    = 0",
        "MPLANCK  = 2.4D18",
        "MGOLD    = 1.D-13",
        "******************* VARIATION OF HIGGS COUPLINGS *************************",
        "ELWK     = 0",
        "CW       = 1.D0",
        "CZ       = 1.D0",
        "Ctau     = 1.D0",
        "Cmu      = 1.D0",
        "Ct       = 1.D0",
        "Cb       = 1.D0",
        "Cc       = 1.D0",
        "Cs       = 1.D0",
        "Cgaga    = 0.D0",
        "Cgg      = 0.D0",
        "CZga     = 0.D0",
        "********************* 4TH GENERATION *************************************",
        "Ctp      = 0.D0",
        "Cbp      = 0.D0",
        "Cnup     = 0.D0",
        "Cep      = 0.D0",
        "********************** real or complex singlet Model *********************",
        "Singlet Extension: 1 - yes, 0 - no (based on SM w/o EW corrections)",
        "Model: 1 - real broken phase, 2 - real dark matter phase",
        "       3 - complex broken phase, 4 - complex dark matter phase",
        "isinglet = 1",
        "icxSM    = 3",
        "*** real singlet broken phase ***",
        "alph1    = 1.",
        "mH1      = 1.",
        "mH2      = 1.",
        "vs       = 1.",
        "*** real singlet dark matter phase ***",
        "mH1      = 1.",
        "mD       = 1.",
        "m2s      = 1.",
        "lambdas  = 3.56328D0",
        "*** complex singlet broken phase ***",
        "alph1    = " + str(row["alph1"]),
        "alph2    = " + str(row["alph2"]),
        "alph3    = " + str(row["alph3"]),
        "m1       = " + str(row["m1"]),
        "m3       = " + str(row["m3"]),
        "vs       = " + str(row["vs"]),
        "*** complex singlet dark matter phase ***",
        "alph1    = -0.317120D0",
        "m1       = 125.3D0",
        "m2       = 400.D0",
        "m3       = 731.205D0",
        "vs       = 522.181D0",
        "a1       = -3.12115D07",
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def write_shdecayin_cd(row, outfile="hdecay_test.in"):
    lines = [
        "SLHAIN   = 0",
        "SLHAOUT  = 0",
        "COUPVAR  = 0",
        "HIGGS    = 0",
        "OMIT ELW = 1",
        "SM4      = 0",
        "FERMPHOB = 0",
        "2HDM     = 0",
        "MODEL    = 1",
        "TGBET    = 30.D0",
        "MABEG    = 100",
        "MAEND    = 100",
        "NMA      = 1",
        "********************* hMSSM (MODEL = 10) *********************************",
        "MHL      = 125.D0",
        "**************************************************************************",
        "ALS(MZ)  = " + str(default_const()[0]),
        "MSBAR(2) = " + str(default_const()[1]),
        "MCBAR(3) = " + str(default_const()[2]),
        "MBBAR(MB)= " + str(default_const()[3]),
        "MT       = " + str(default_const()[4]),
        "MTAU     = " + str(default_const()[5]),
        "MMUON    = " + str(default_const()[6]),
        "1/ALPHA  = " + str(default_const()[7]),
        "GF       = " + str(default_const()[8]),
        "GAMW     = " + str(default_const()[9]),
        "GAMZ     = " + str(default_const()[10]),
        "MZ       = " + str(default_const()[11]),
        "MW       = " + str(default_const()[12]),
        "VTB      = " + str(default_const()[13]),
        "VTS      = " + str(default_const()[14]),
        "VTD      = " + str(default_const()[15]),
        "VCB      = " + str(default_const()[16]),
        "VCS      = " + str(default_const()[17]),
        "VCD      = " + str(default_const()[18]),
        "VUB      = " + str(default_const()[19]),
        "VUS      = " + str(default_const()[20]),
        "VUD      = " + str(default_const()[21]),
        "********************* 4TH GENERATION *************************************",
        "  SCENARIO FOR ELW. CORRECTIONS TO H -> GG (EVERYTHING IN GEV):",
        "  GG_ELW = 1: MTP = 500    MBP = 450    MNUP = 375    MEP = 450",
        "  GG_ELW = 2: MBP = MNUP = MEP = 600    MTP = MBP+50*(1+LOG(M_H/115)/5)",
        "",
        "GG_ELW   = 1",
        "MTP      = 500.D0",
        "MBP      = 450.D0",
        "MNUP     = 375.D0",
        "MEP      = 450.D0",
        "************************** 2 Higgs Doublet Model *************************",
        "  TYPE: 1 (I), 2 (II), 3 (lepton-specific), 4 (flipped)",
        "  PARAM: 1 (masses), 2 (lambda_i)",
        "",
        "PARAM    = 1",
        "TYPE     = 1",
        "********************",
        "TGBET2HDM= 1.",
        "M_12^2   = 1.",
        "******************** PARAM=1:",
        "ALPHA_H  = 1.",
        "MHL      = 1.",
        "MHH      = 1.",
        "MHA      = 1.",
        "MH+-     = 1.",
        "******************** PARAM=2:",
        "LAMBDA1  = 2.6885665050462264D0",
        "LAMBDA2  = 0.000156876030254505681D0",
        "LAMBDA3  = 0.46295674052962260D0",
        "LAMBDA4  = 0.96605498373771792D0",
        "LAMBDA5  = -0.88138084173680198D0",
        "**************************************************************************",
        "SUSYSCALE= 1000.D0",
        "MU       = 1000.D0",
        "M2       = 1000.D0",
        "MGLUINO  = 1000.D0",
        "MSL1     = 1000.D0",
        "MER1     = 1000.D0",
        "MQL1     = 1000.D0",
        "MUR1     = 1000.D0",
        "MDR1     = 1000.D0",
        "MSL      = 1000.D0",
        "MER      = 1000.D0",
        "MSQ      = 1000.D0",
        "MUR      = 1000.D0",
        "MDR      = 1000.D0",
        "AL       = 2450.D0",
        "AU       = 2450.D0",
        "AD       = 2450.D0",
        "NNLO (M) = 1",
        "ON-SHELL = 0",
        "ON-SH-WZ = 0",
        "IPOLE    = 0",
        "OFF-SUSY = 0",
        "INDIDEC  = 0",
        "NF-GG    = 5",
        "IGOLD    = 0",
        "MPLANCK  = 2.4D18",
        "MGOLD    = 1.D-13",
        "******************* VARIATION OF HIGGS COUPLINGS *************************",
        "ELWK     = 0",
        "CW       = 1.D0",
        "CZ       = 1.D0",
        "Ctau     = 1.D0",
        "Cmu      = 1.D0",
        "Ct       = 1.D0",
        "Cb       = 1.D0",
        "Cc       = 1.D0",
        "Cs       = 1.D0",
        "Cgaga    = 0.D0",
        "Cgg      = 0.D0",
        "CZga     = 0.D0",
        "********************* 4TH GENERATION *************************************",
        "Ctp      = 0.D0",
        "Cbp      = 0.D0",
        "Cnup     = 0.D0",
        "Cep      = 0.D0",
        "********************** real or complex singlet Model *********************",
        "Singlet Extension: 1 - yes, 0 - no (based on SM w/o EW corrections)",
        "Model: 1 - real broken phase, 2 - real dark matter phase",
        "       3 - complex broken phase, 4 - complex dark matter phase",
        "isinglet = 1",
        "icxSM    = 4",
        "*** real singlet broken phase ***",
        "alph1    = 1.",
        "mH1      = 1.",
        "mH2      = 1.",
        "vs       = 1.",
        "*** real singlet dark matter phase ***",
        "mH1      = 1.",
        "mD       = 1.",
        "m2s      = 1.",
        "lambdas  = 3.56328D0",
        "*** complex singlet broken phase ***",
        "alph1    = ",
        "alph2    = 1.",
        "alph3    = 1.",
        "m1       = 1.",
        "m3       = 1.",
        "vs       = 1.",
        "*** complex singlet dark matter phase ***",
        "alph1    = " + str(row["alph1"]),
        "m1       = " + str(row["m1"]),
        "m2       = " + str(row["m2"]),
        "m3       = " + str(row["m3"]),
        "vs       = " + str(row["vs"]),
        "a1       = " + str(row["a1"]),
    ]
    with open(outfile, "w") as f:
        f.write("\n".join(lines))


def read_n2hdecay_broken(path):
    A_files = ["br.A_N2HDM_a", "br.A_N2HDM_b", "br.A_N2HDM_c"]
    Hp_files = ["br.H+_N2HDM_a", "br.H+_N2HDM_b", "br.H+_N2HDM_c"]
    H1_files = ["br.H1_N2HDM_a", "br.H1_N2HDM_b", "br.H1_N2HDM_c"]
    H2_files = ["br.H2_N2HDM_a", "br.H2_N2HDM_b", "br.H2_N2HDM_c"]
    H3_files = ["br.H3_N2HDM_a", "br.H3_N2HDM_b", "br.H3_N2HDM_c", "br.H3_N2HDM_d"]
    t_files = ["br.top"]
    BR_A = []
    BR_Hp = []
    BR_H1 = []
    BR_H2 = []
    BR_H3 = []
    BR_t = []
    readlist = [
        [BR_A, A_files],
        [BR_Hp, Hp_files],
        [BR_H1, H1_files],
        [BR_H2, H2_files],
        [BR_H3, H3_files],
        [BR_t, t_files],
    ]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_A_n = [
        "b_A_bb",
        "b_A_tautau",
        "b_A_mumu",
        "b_A_ss",
        "b_A_cc",
        "b_A_tt",
        "b_A_gg",
        "b_A_gamgam",
        "b_A_Zgam",
        "b_A_ZH1",
        "b_A_ZH2",
        "b_A_ZH3",
        "b_A_WpHm",
        "w_A",
    ]
    BR_Hp_n = [
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WH3",
        "b_Hp_WA",
        "w_Hp",
    ]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_AA",
        "b_H1_ZA",
        "b_H1_WpHm",
        "b_H1_HpHm",
        "w_H1",
    ]
    BR_H2_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_H1H1",
        "b_H2_AA",
        "b_H2_ZA",
        "b_H2_WpHm",
        "b_H2_HpHm",
        "w_H2",
    ]
    BR_H3_n = [
        "b_H3_bb",
        "b_H3_tautau",
        "b_H3_mumu",
        "b_H3_ss",
        "b_H3_cc",
        "b_H3_tt",
        "b_H3_gg",
        "b_H3_gamgam",
        "b_H3_Zgam",
        "b_H3_WW",
        "b_H3_ZZ",
        "b_H3_H1H1",
        "b_H3_H1H2",
        "b_H3_H2H2",
        "b_H3_AA",
        "b_H3_ZA",
        "b_H3_WpHm",
        "b_H3_HpHm",
        "w_H3",
    ]
    BR_t_n = ["b_t_Wb", "b_t_Hpb", "w_t"]
    return pd.Series(
        dict(
            zip(
                BR_A_n + BR_Hp_n + BR_H1_n + BR_H2_n + BR_H3_n + BR_t_n,
                BR_A + BR_Hp + BR_H1 + BR_H2 + BR_H3 + BR_t,
            )
        ),
        dtype=np.double,
    )


def read_n2hdecay_darks(path):
    A_files = ["br.A_N2HDM_a", "br.A_N2HDM_b", "br.A_N2HDM_c"]
    Hp_files = ["br.H+_N2HDM_a", "br.H+_N2HDM_b", "br.H+_N2HDM_c"]
    H2_files = ["br.H2_N2HDM_a", "br.H2_N2HDM_b", "br.H2_N2HDM_c", "br.H2_N2HDM_d"]
    H1_files = ["br.H1_N2HDM_a", "br.H1_N2HDM_b", "br.H1_N2HDM_c"]
    t_files = ["br.top"]
    BR_A = []
    BR_Hp = []
    BR_H2 = []
    BR_H1 = []
    BR_t = []
    readlist = [
        [BR_A, A_files],
        [BR_Hp, Hp_files],
        [BR_H1, H1_files],
        [BR_H2, H2_files],
        [BR_t, t_files],
    ]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_A_n = [
        "b_A_bb",
        "b_A_tautau",
        "b_A_mumu",
        "b_A_ss",
        "b_A_cc",
        "b_A_tt",
        "b_A_gg",
        "b_A_gamgam",
        "b_A_Zgam",
        "b_A_ZH1",
        "b_A_ZH2",
        "b_A_WpHm",
        "w_A",
    ]
    BR_Hp_n = [
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WA",
        "w_Hp",
    ]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_HDHD",
        "b_H1_AA",
        "b_H1_ZA",
        "b_H1_WpHm",
        "b_H1_HpHm",
        "w_H1",
    ]
    BR_H2_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_HDHD",
        "b_H2_H1H1",
        "b_H2_AA",
        "b_H2_ZA",
        "b_H2_WpHm",
        "b_H2_HpHm",
        "w_H2",
    ]
    BR_t_n = ["b_t_Wb", "b_t_Hpb", "w_t"]
    return pd.Series(
        dict(
            zip(
                BR_A_n + BR_Hp_n + BR_H1_n + BR_H2_n + BR_t_n,
                BR_A + BR_Hp + BR_H1 + BR_H2 + BR_t,
            )
        ),
        dtype=np.double,
    )


def read_n2hdecay_darkd(path):
    H2_files = ["br.H2_N2HDM_a", "br.H2_N2HDM_b", "br.H2_N2HDM_c", "br.H2_N2HDM_d"]
    H1_files = ["br.H1_N2HDM_a", "br.H1_N2HDM_b", "br.H1_N2HDM_c"]
    HD_files = ["br.HD_N2HDM"]
    AD_files = ["br.AD_N2HDM"]
    HDc_files = ["br.HD+_N2HDM"]
    BR_H2 = []
    BR_H1 = []
    BR_HD = []
    BR_AD = []
    BR_HDc = []
    readlist = [
        [BR_H1, H1_files],
        [BR_H2, H2_files],
        [BR_HD, HD_files],
        [BR_AD, AD_files],
        [BR_HDc, HDc_files],
    ]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_HDHD",
        "b_H1_ADAD",
        "b_H1_ZAD",
        "b_H1_WpHDm",
        "b_H1_HDpHDm",
        "w_H1",
    ]
    BR_H2_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_HDHD",
        "b_H2_H1H1",
        "b_H2_ADAD",
        "b_H2_ZAD",
        "b_H2_WpHDm",
        "b_H2_HDpHDm",
        "w_H2",
    ]
    BR_HD_n = ["b_HD_ZAD", "b_HD_WpHDm", "w_HD"]
    BR_AD_n = ["b_AD_ZHD", "b_AD_WpHDm", "w_AD"]
    BR_HDc_n = ["b_HDc_WHD", "b_HDc_WAD", "w_HDc"]
    return pd.Series(
        dict(
            zip(
                BR_H1_n + BR_H2_n + BR_HD_n + BR_AD_n + BR_HDc_n,
                BR_H1 + BR_H2 + BR_HD + BR_AD + BR_HDc,
            )
        ),
        dtype=np.double,
    )


def read_n2hdecay_darksd(path):
    Hsm_files = ["br.Hsm_N2HDM_a", "br.Hsm_N2HDM_b", "br.Hsm_N2HDM_c"]
    HDD_files = ["br.HDD_N2HDM"]
    AD_files = ["br.AD_N2HDM"]
    HDc_files = ["br.HD+_N2HDM"]
    BR_Hsm = []
    BR_HDD = []
    BR_AD = []
    BR_HDc = []
    readlist = [
        [BR_Hsm, Hsm_files],
        [BR_HDD, HDD_files],
        [BR_AD, AD_files],
        [BR_HDc, HDc_files],
    ]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]

    BR_Hsm_n = [
        "b_Hsm_bb",
        "b_Hsm_tautau",
        "b_Hsm_mumu",
        "b_Hsm_ss",
        "b_Hsm_cc",
        "b_Hsm_tt",
        "b_Hsm_gg",
        "b_Hsm_gamgam",
        "b_Hsm_Zgam",
        "b_Hsm_WW",
        "b_Hsm_ZZ",
        "b_Hsm_HDDHDD",
        "b_Hsm_ADAD",
        "b_Hsm_HDpHDm",
        "b_Hsm_HDSHDS",
        "w_Hsm",
    ]
    BR_HDD_n = ["b_HDD_ZAD", "b_HDD_WpHDm", "w_HDD"]
    BR_AD_n = ["b_AD_ZHDD", "b_AD_WpHDm", "w_AD"]
    BR_HDc_n = ["b_HDc_WHDD", "b_HDc_WAD", "w_HDc"]
    return pd.Series(
        dict(
            zip(
                BR_Hsm_n + BR_HDD_n + BR_AD_n + BR_HDc_n,
                BR_Hsm + BR_HDD + BR_AD + BR_HDc,
            )
        ),
        dtype=np.double,
    )


def read_hdecay_2hdm(path):
    A_files = ["br.a1_2HDM", "br.a2_2HDM", "br.a3_2HDM"]
    Hp_files = ["br.c1_2HDM", "br.c2_2HDM", "br.c3_2HDM"]
    Hh_files = ["br.h1_2HDM", "br.h2_2HDM", "br.h3_2HDM"]
    Hl_files = ["br.l1_2HDM", "br.l2_2HDM", "br.l3_2HDM"]
    t_files = ["br.top"]
    BR_A = []
    BR_Hp = []
    BR_Hh = []
    BR_Hl = []
    BR_t = []
    readlist = [
        [BR_A, A_files],
        [BR_Hp, Hp_files],
        [BR_Hl, Hl_files],
        [BR_Hh, Hh_files],
        [BR_t, t_files],
    ]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_A_n = [
        "b_A_bb",
        "b_A_tautau",
        "b_A_mumu",
        "b_A_ss",
        "b_A_cc",
        "b_A_tt",
        "b_A_gg",
        "b_A_gamgam",
        "b_A_Zgam",
        "b_A_ZH1",
        "b_A_ZH2",
        "b_A_WpHm",
        "w_A",
    ]
    BR_Hp_n = [
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WA",
        "w_Hp",
    ]
    BR_Hl_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_AA",
        "b_H1_ZA",
        "b_H1_WpHm",
        "b_H1_HpHm",
        "w_H1",
    ]
    BR_Hh_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_H1H1",
        "b_H2_AA",
        "b_H2_ZA",
        "b_H2_WpHm",
        "b_H2_HpHm",
        "w_H2",
    ]
    BR_t_n = ["b_t_Wb", "b_t_Hpb", "w_t"]
    return pd.Series(
        dict(
            zip(
                BR_A_n + BR_Hp_n + BR_Hl_n + BR_Hh_n + BR_t_n,
                BR_A + BR_Hp + BR_Hl + BR_Hh + BR_t,
            )
        ),
        dtype=np.double,
    )


def read_hdecay_sm(path):
    H_files = ["br.sm1", "br.sm2"]
    BR_H = []
    readlist = [[BR_H, H_files]]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_H_n = [
        "b_H_bb",
        "b_H_tautau",
        "b_H_mumu",
        "b_H_ss",
        "b_H_cc",
        "b_H_tt",
        "b_H_gg",
        "b_H_gamgam",
        "b_H_Zgam",
        "b_H_WW",
        "b_H_ZZ",
        "w_H",
    ]
    return pd.Series(dict(zip(BR_H_n, BR_H)), dtype=np.double)


def read_chdecay_c2hdm(path):
    Hp_files = ["br.c1_C2HDM", "br.c2_C2HDM", "br.c3_C2HDM"]
    H1_files = ["br.H1a_C2HDM", "br.H1b_C2HDM", "br.H1c_C2HDM"]
    H2_files = ["br.H2a_C2HDM", "br.H2b_C2HDM", "br.H2c_C2HDM"]
    H3_files = ["br.H3a_C2HDM", "br.H3b_C2HDM", "br.H3c_C2HDM", "br.H3d_C2HDM"]
    t_files = ["br.top"]
    BR_Hp = []
    BR_H1 = []
    BR_H2 = []
    BR_H3 = []
    BR_t = []
    readlist = [
        [BR_Hp, Hp_files],
        [BR_H1, H1_files],
        [BR_H2, H2_files],
        [BR_H3, H3_files],
        [BR_t, t_files],
    ]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_Hp_n = [
        "b_Hp_cb",
        "b_Hp_taunu",
        "b_Hp_munu",
        "b_Hp_us",
        "b_Hp_cs",
        "b_Hp_tb",
        "b_Hp_cd",
        "b_Hp_ub",
        "b_Hp_ts",
        "b_Hp_td",
        "b_Hp_WH1",
        "b_Hp_WH2",
        "b_Hp_WH3",
        "w_Hp",
    ]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "b_H1_WpHm",
        "b_H1_HpHm",
        "w_H1",
    ]
    BR_H2_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "b_H2_ZH1",
        "b_H2_WpHm",
        "b_H2_H1H1",
        "b_H2_HpHm",
        "w_H2",
    ]
    BR_H3_n = [
        "b_H3_bb",
        "b_H3_tautau",
        "b_H3_mumu",
        "b_H3_ss",
        "b_H3_cc",
        "b_H3_tt",
        "b_H3_gg",
        "b_H3_gamgam",
        "b_H3_Zgam",
        "b_H3_WW",
        "b_H3_ZZ",
        "b_H3_ZH1",
        "b_H3_ZH2",
        "b_H3_WpHm",
        "b_H3_H1H1",
        "b_H3_H1H2",
        "b_H3_H2H2",
        "b_H3_HpHm",
        "w_H3",
    ]
    BR_t_n = ["b_t_Wb", "b_t_Hpb", "w_t"]
    return pd.Series(
        dict(
            zip(
                BR_Hp_n + BR_H1_n + BR_H2_n + BR_H3_n + BR_t_n,
                BR_Hp + BR_H1 + BR_H2 + BR_H3 + BR_t,
            )
        ),
        dtype=np.double,
    )


def read_shdecay_rb(path):
    H1_files = ["br.rb11", "br.rb12", "br.rb13"]
    H2_files = ["br.rb21", "br.rb22", "br.rb23"]
    BR_H1 = []
    BR_H2 = []
    readlist = [[BR_H1, H1_files], [BR_H2, H2_files]]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "w_H1",
        "b_H1_H2H2",
    ]
    BR_H2_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "w_H2",
        "b_H2_H1H1",
    ]
    return pd.Series(dict(zip(BR_H1_n + BR_H2_n, BR_H1 + BR_H2)), dtype=np.double)


def read_shdecay_rd(path):
    H1_files = ["br.rd11", "br.rd12", "br.rd13"]
    BR_H1 = []
    readlist = [[BR_H1, H1_files]]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "w_H1",
        "b_H1_HDHD",
    ]

    return pd.Series(dict(zip(BR_H1_n, BR_H1)), dtype=np.double)


def read_shdecay_cb(path):
    H1_files = ["br.cb11", "br.cb12", "br.cb13"]
    H2_files = ["br.cb21", "br.cb22", "br.cb23"]
    H3_files = ["br.cb31", "br.cb32", "br.cb33"]
    BR_H1 = []
    BR_H2 = []
    BR_H3 = []
    readlist = [[BR_H1, H1_files], [BR_H2, H2_files], [BR_H3, H3_files]]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "w_H1",
        "b_H1_H2H2",
        "b_H1_H2H3",
        "b_H1_H3H3",
    ]
    BR_H2_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "w_H2",
        "b_H2_H1H1",
        "b_H2_H1H3",
        "b_H2_H3H3",
    ]
    BR_H3_n = [
        "b_H3_bb",
        "b_H3_tautau",
        "b_H3_mumu",
        "b_H3_ss",
        "b_H3_cc",
        "b_H3_tt",
        "b_H3_gg",
        "b_H3_gamgam",
        "b_H3_Zgam",
        "b_H3_WW",
        "b_H3_ZZ",
        "w_H3",
        "b_H3_H1H1",
        "b_H3_H1H2",
        "b_H3_H2H2",
    ]
    return pd.Series(
        dict(zip(BR_H1_n + BR_H2_n + BR_H3_n, BR_H1 + BR_H2 + BR_H3)), dtype=np.double
    )


def read_shdecay_cd(path):
    H1_files = ["br.cd11", "br.cd12", "br.cd13"]
    H2_files = ["br.cd21", "br.cd22", "br.cd23"]
    BR_H1 = []
    BR_H2 = []
    readlist = [[BR_H1, H1_files], [BR_H2, H2_files]]
    for BR, files in readlist:
        for fname in files:
            with open(os.path.join(path, fname)) as ifile:
                line = ifile.readlines()[3]
                BR += line.split()[1:]
    BR_H1_n = [
        "b_H1_bb",
        "b_H1_tautau",
        "b_H1_mumu",
        "b_H1_ss",
        "b_H1_cc",
        "b_H1_tt",
        "b_H1_gg",
        "b_H1_gamgam",
        "b_H1_Zgam",
        "b_H1_WW",
        "b_H1_ZZ",
        "w_H1",
        "b_H1_H2H2",
        "b_H1_H2HD",
        "b_H1_HDHD",
    ]
    BR_H2_n = [
        "b_H2_bb",
        "b_H2_tautau",
        "b_H2_mumu",
        "b_H2_ss",
        "b_H2_cc",
        "b_H2_tt",
        "b_H2_gg",
        "b_H2_gamgam",
        "b_H2_Zgam",
        "b_H2_WW",
        "b_H2_ZZ",
        "w_H2",
        "b_H2_H1H1",
        "b_H2_H1HD",
        "b_H2_HDHD",
    ]

    return pd.Series(dict(zip(BR_H1_n + BR_H2_n, BR_H1 + BR_H2)), dtype=np.double)


def random_n2hdm_broken(npoints):
    def point():
        return (
            [r(1e-3, 50), r(-1e6, 1e6)]
            + list(r(10, 2e3, 2))
            + list(sorted(r(10, 2e3, 3)))
            + list(r(-np.pi / 2, np.pi / 2, 3))
            + [r(10, 2e3), int(r(1, 4.99))]
        )

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=[
            "tbeta",
            "m12sq",
            "mA",
            "mHp",
            "mH1",
            "mH2",
            "mH3",
            "a1",
            "a2",
            "a3",
            "vs",
            "type",
        ],
    )


def random_n2hdm_darks(npoints):
    def point():
        return (
            [r(1e-3, 50), r(-1e6, 1e6)]
            + list(r(10, 2e3, 3))
            + sorted(r(10, 2e3, 2), reverse=True)
            + [r(-np.pi / 2, np.pi / 2)]
            + list(r(-20, 20, 2))
            + [int(r(1, 4.99))]
        )

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=[
            "tbeta",
            "m12sq",
            "mA",
            "mHp",
            "mHD",
            "mH2",
            "mH1",
            "alpha",
            "L7",
            "L8",
            "type",
        ],
    )


def random_n2hdm_darkd(npoints):
    def point():
        return (
            list(r(10, 2e3, 3))
            + sorted(r(10, 2e3, 2), reverse=True)
            + [r(-np.pi / 2, np.pi / 2)]
            + [r(-20, 20), r(-1e6, 1e6), r(10, 2e3)]
        )

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=["mA", "mHp", "mHD", "mH2", "mH1", "alpha", "L8", "m22sq", "vs"],
    )


def random_n2hdm_darksd(npoints):
    def point():
        return list(r(10, 2e3, 5)) + list(r(1e-6, 1e6, 2))

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=["mA", "mHp", "mHDD", "mHsm", "mHDS", "m22sq", "mssq"],
    )


def random_2hdm(npoints):
    def point():
        return (
            [r(1e-3, 50), r(-1e6, 1e6)]
            + list(r(10, 2e3, 2))
            + sorted(r(10, 2e3, 2), reverse=True)
            + [r(-np.pi / 2, np.pi / 2)]
            + [int(r(1, 4.99))]
        )

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=["tbeta", "m12sq", "mA", "mHp", "mH2", "mH1", "alpha", "type"],
    )


def random_sm(npoints):
    def point():
        return [r(10, 1e3)]

    return pd.DataFrame(data=[point() for n in range(npoints)], columns=["mH"])


def random_rxsm_b(npoints):
    def point():
        return [r(-np.pi / 2, np.pi / 2), r(10, 1e3), r(10, 1e3), r(10, 2e3)]

    return pd.DataFrame(
        data=[point() for n in range(npoints)], columns=["alph1", "mH1", "mH2", "vs"]
    )


def random_rxsm_d(npoints):
    def point():
        return [r(10, 1e3), r(10, 1e3), r(10, 2e3)]

    return pd.DataFrame(
        data=[point() for n in range(npoints)], columns=["mH1", "mD", "m2s"]
    )


def random_cxsm_b(npoints):
    def point():
        return list(r(-np.pi / 2, np.pi / 2, 3)) + [r(10, 1e3), r(10, 1e3), r(10, 2e3)]

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=["alph1", "alph2", "alph3", "m1", "m3", "vs"],
    )


def random_cxsm_d(npoints):
    def point():
        return [
            r(-np.pi / 2, np.pi / 2),
            r(10, 1e3),
            r(10, 1e3),
            r(10, 1e3),
            r(10, 2e3),
            r(-100, 100),
        ]

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=["alph1", "m1", "m2", "m3", "vs", "a1"],
    )


def random_c2hdm(npoints):
    def point():
        return (
            [r(1e-3, 50), r(-1e6, 1e6), r(10, 2e3)]
            + sorted(r(10, 2e3, 2))
            + list(r(-np.pi / 2, np.pi / 2, 3))
            + [int(r(1, 4.99))]
        )

    return pd.DataFrame(
        data=[point() for n in range(npoints)],
        columns=["tbeta", "m12sq", "mHp", "mH1", "mH2", "a1", "a2", "a3", "type"],
    )


# %%
if __name__ == "__main__":
    main()
