#include "AnyHdecay.hpp"
#include "catch.hpp"
#include <map>

using namespace AnyHdecay;

TEST_CASE("SM single point", "[SM]") {

    SECTION("default constants") {
        auto hdec = Hdecay{};

        auto res = hdec.sm(HMass{125.09});

        CHECK(res["w_H"] == Approx(4.0796577e-3));
        CHECK(res["b_H_bb"] == Approx(0.5900728083));
        CHECK(res["b_H_tautau"] == Approx(0.0634562628));
        CHECK(res["b_H_mumu"] == Approx(0.0002246568));
        CHECK(res["b_H_ss"] == Approx(0.0002228355));
        CHECK(res["b_H_cc"] == Approx(0.0288878842));
        CHECK(res["b_H_tt"] == 0.);
        CHECK(res["b_H_gg"] == Approx(0.0782275488));
        CHECK(res["b_H_gamgam"] == Approx(0.0023182132));
        CHECK(res["b_H_Zgam"] == Approx(0.0015478927));
        CHECK(res["b_H_WW"] == Approx(0.2089140756));
        CHECK(res["b_H_ZZ"] == Approx(0.026127822));
    }

    SECTION("modified constants") {
        auto p = SMParameters{};
        p.alphaS = 0.12;
        p.mS = 0.1;
        p.mC = 0.1;
        p.mB = 4.;
        p.mT = 175;
        p.mTau = 2.;
        p.mMu = 0.1;
        p.invAlpha = 138.;
        p.GF = 1.2e-5;
        p.widthW = 2.;
        p.widthZ = 2.5;
        p.mZ = 90;
        p.mW = 80;
        p.ckmTB = 1.;
        p.ckmTS = 1.;
        p.ckmTD = 1.;
        p.ckmCB = 1.;
        p.ckmCS = 1.;
        p.ckmCD = 1.;
        p.ckmUB = 1.;
        p.ckmUS = 1.;
        p.ckmUD = 1.;

        auto hdec = Hdecay(p);

        auto res = hdec.sm(HMass{125.09});

        CHECK(res["w_H"] == Approx(3.9091746e-3));
        CHECK(res["b_H_bb"] == Approx(0.5612227776));
        CHECK(res["b_H_tautau"] == Approx(0.0862953585));
        CHECK(res["b_H_mumu"] == Approx(0.0002160689));
        CHECK(res["b_H_ss"] == Approx(0.0002568116));
        CHECK(res["b_H_cc"] == Approx(0.0003346348));
        CHECK(res["b_H_tt"] == 0.);
        CHECK(res["b_H_gg"] == Approx(0.0876012299));
        CHECK(res["b_H_gamgam"] == Approx(0.0024706038));
        CHECK(res["b_H_Zgam"] == Approx(0.0018769428));
        CHECK(res["b_H_WW"] == Approx(0.2262752305));
        CHECK(res["b_H_ZZ"] == Approx(0.0334503415));
    }
}

