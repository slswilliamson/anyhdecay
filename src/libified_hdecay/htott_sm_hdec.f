!     Extracted from hdecay 6.60 on 12.03.2021
      subroutine htotts_hdec(amh,amt,amb,amw,htts)
      implicit real*8(a-z)
      integer ip,k
      common/prec1_hdec/ip
      external funstt1_hdec
      common/iksy0_hdec/x1,x2,m1,m2,m3,ecm,s
      common/top0_hdec/amh0,amt0,amb0,amw0
      amh0=amh
      amt0=amt
      amb0=amb
      amw0=amw
      ip=5
      m1=amb
      m2=amt
      m3=amw
c     first integrate over x2, i.e. (1+3) system
c        check whether enough phase space
      mastot=m1+m2+m3
      if(mastot.ge.amh) goto 12
      ecm=amh
      s=ecm**2
      u1=(ecm-m2)**2
      d1=(m1+m3)**2
      u=(s-d1+m2**2)/s
      d=(s-u1+m2**2)/s
      del=(u-d)/ip
      u=d+del
      xsec=0.d0
      do k=1,ip
      call qgaus1_hdec(funstt1_hdec,d,u,ss)
      d=u
      u=d+del
      xsec=xsec+ss
      enddo
      htts=xsec
12    continue
      return
      end


      double precision function funstt1_hdec(xl)
      implicit real*8(a-z)
      integer ip,i
      common/iksy0_hdec/x1,x2,m1,m2,m3,ecm,s
      common/prec1_hdec/ip
      external funstt2_hdec
      x2=xl
      s13=s-s*x2+m2**2
      tem=2.d0*dsqrt(s13)
      e2s=(s-s13-m2**2)/tem
      e3s=(s13+m3**2-m1**2)/tem
c     second integral over x1, i.e. (2+3) system
      u1=(e2s+e3s)**2-(dsqrt(e2s**2-m2**2)-dsqrt(e3s**2-m3**2))**2
      d1=(e2s+e3s)**2-(dsqrt(e2s**2-m2**2)+dsqrt(e3s**2-m3**2))**2
      u=(s-d1+m1**2)/s
      d=(s-u1+m1**2)/s
      del=(u-d)/ip
      funstt1_hdec=0.d0
      u=d+del
      do i=1,ip
      call qgaus2_hdec(funstt2_hdec,d,u,ss)
      funstt1_hdec=funstt1_hdec+ss
      d=u
      u=d+del
      enddo
      return
      end

c --- this is for a check of the off-shell decays h/h -> h+ w-

      double precision function funstt2_hdec(xk)
      implicit real*8(a-z)
      common/iksy0_hdec/x1,x2,m1,m2,m3,ecm,s
      x1=xk
      call elemstt_hdec(ss)
      funstt2_hdec=ss
      return
      end


      subroutine elemstt_hdec(res)
      implicit real*8(a-z)
      common/iksy0_hdec/x1,x2,m1,m2,m3,ecm,s
      common/top0_hdec/amh,amt,amb,amw
      common/wzwdth_hdec/gamc0,gamt0,gamt1,gamw0,gamz0
      gamt=gamt0**2*amt**2/amh**4
      gamw=gamw0**2*amw**2/amh**4
      w=amw**2/amh**2
      t=amt**2/amh**2
      y1=1-x2
      y2=1-x1
      x0=2.d0-x1-x2
      w1=(1.d0-x2)
      w3=(1.d0-x1-x2)
      w11=1.d0/((1.d0-x2)**2+gamt)
      w33=1.d0/(w3**2+gamw)
      w13=w1*w3*w11*w33

      r11=4*t*w-16.*t*w*y1-4.*t*y2*y1+8.*t*y1+32.*t*w**2-20
     . .*t*y1**2+8.*w*y2*y1+4.*w*y1**2-4.*y2*y1**2-16.*t**2*w-
     .  32.*t**2*y1+4.*t**2-16.*t**3-8.*w**2+4.*y1**2-4.*y1**3
      r33=-4.*t*w+4.*t*w*y2-2.*t*w*y2*y1+4.*t*w*y1+t*w*y2**2-
     .  3.*t*w*y1**2+2.*t*y2*y1-3.*t*y2*y1**2+4.*t*w**2-4.*t*w**3
     .  +t*y2**2-3.*t*y2**2*y1-t*y2**3+t*y1**2-t*y1**3+4.*t**2
     .  *w-4.*t**2*w*y2-4.*t**2*w*y1-2.*t**2*y2*y1-4.*t**2*w**2-
     .  t**2*y2**2-t**2*y1**2+4.*w**2*y2*y1-8.*w**3*y2-8.*w**3*y1
     .  +4.*w**3+8.*w**4
      r13=8.*w-24.*t*w+16.*t*w*y1 -4.*t*y2+16.*t*y2*y1-4.*t*
     .  y1+16.*t*w**2+4.*t*y2**2+12.*t*y1**2-8.*w*y2-12.*w*y2*y1
     .  -8.*w*y1+4.*w*y1**2-4.*y2*y1+8.*y2*y1**2+16.*t**2*w+8.
     .  *t**2*y2+8.*t**2*y1+16.*w**2*y2+24.*w**2*y1+4.*y2**2*y1-
     .  32.*w**3-4.*y1**2+4.*y1**3
      res=r11*w11+4.d0*r33*w33/t-2.d0*r13*w13
      return
      end

c     **************************************************
c     subroutine for a -> tt* -> tbw
c     **************************************************
