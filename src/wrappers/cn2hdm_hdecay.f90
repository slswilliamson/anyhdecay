subroutine hdecay_CN2HDM(type2HDM, vevS, tbeta, re_m12sq, mHc, mHD, &
                        mH1, mH2, mH3, mH4, &
                        L1, L2, L3, L4, RL5, &
			            L6, L7, L8, IL5, &
			            a1, a2, a3, a4, a5, a6, &
                        smparams, &
                        BR) bind(C, name='hdecay_CN2HDM')
    use, intrinsic :: iso_c_binding

    implicit none
    integer(kind=c_int), intent(in), value :: type2HDM
    real(kind=c_double), intent(in), value :: vevS, tbeta, re_m12sq, mHc, mHD, mH1, mH2, mH3, mH4
    real(kind=c_double), intent(in), value :: L1, L2, L3, L4, RL5, L6, L7, L8, IL5
    real(kind=c_double), intent(in), value :: a1, a2, a3, a4, a5, a6
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) :: BR(94)


! common block variables from read_hdec
    double precision ams, amc, amb, amt
    double precision AMSB
    double precision amcb, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision AMSM, AMA, AML, AMH, AMCH, AMAR
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0, IHIGGS, NNLO, IPOLE
    integer IONSH, IONWZ, IOFSUSY, NFGG
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    double precision AXMPL, AXMGD
    integer ICOUPELW
    integer IFERMPHOB
    integer IMODEL
    integer IGOLD
    integer INDIDEC
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    double precision TGBET2HDM, ALPH2HDM, AMHL2HDM, AMHH2HDM, &
        AMHA2HDM, AMHC2HDM, AM12SQ, A1LAM2HDM, A2LAM2HDM, A3LAM2HDM, &
        A4LAM2HDM, A5LAM2HDM
    integer ITYPE2HDM, I2HDM, IPARAM2HDM
    integer IOELW
    double precision VS
    double precision TGBETCN2HDM
    double precision AMI_CN2HDM(6),Rij_CN2HDM(4,4), ALAM_CN2HDM(9), AMCH_CN2HDM
    integer ITYPECN2HDM, ICN2HDM
    double precision R11, R12, R13, R14, R21, R22, R23, R24, R31, &
	    R32, R33, R34, R41, R42, R43, R44
    double precision c1, c2, c3, c4, c5, c6, s1, s2, s3, s4, s5, s6
    COMMON/MASSES_HDEC/AMS,AMC,AMB,AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/PARAM_HDEC/GF,ALPH,AMTAU,AMMUON,AMZ,AMW
    COMMON/CKMPAR_HDEC/VTB,VTS,VTD,VCB,VCS,VCD,VUB,VUS,VUD
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/HMASS_HDEC/AMSM,AMA,AML,AMH,AMCH,AMAR
    COMMON/WZWDTH_HDEC/GAMC0,GAMT0,GAMT1,GAMW,GAMZ
    COMMON/ALS_HDEC/XLAMBDA,AMC0,AMB0,AMT0,N0
    COMMON/FLAG_HDEC/IHIGGS,NNLO,IPOLE
    COMMON/MODEL_HDEC/IMODEL
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/ONSHELL_HDEC/IONSH,IONWZ,IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/SM4_HDEC/AMTP,AMBP,AMNUP,AMEP,ISM4,IGGELW
    COMMON/GOLDST_HDEC/AXMPL,AXMGD,IGOLD
    COMMON/FLAGS_HDEC/INDIDEC
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
          CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/THDM_HDEC/TGBET2HDM,ALPH2HDM,AMHL2HDM,AMHH2HDM, &
          AMHA2HDM,AMHC2HDM,AM12SQ,A1LAM2HDM,A2LAM2HDM,A3LAM2HDM, &
          A4LAM2HDM,A5LAM2HDM,ITYPE2HDM,I2HDM,IPARAM2HDM
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/CN2HDM_HDEC/TGBETCN2HDM,VS,AMI_CN2HDM,Rij_CN2HDM, &
          ALAM_CN2HDM,AMCH_CN2HDM
    COMMON/CN2HDMI_HDEC/ITYPECN2HDM,ICN2HDM

! local variables
    double precision tgbet, alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber

! common block variables from WRITE_HDEC
    double precision hibrt(4),hibrb(4),hibrl(4),hibrm(4), &
        hibrs(4),hibrc(4),hibrg(4),hibrga(4),hibrzga(4),hibrw(4),hibrhdhd(4), &
        hibrz(4),hibrhpwm(4),hibrchch(4),hibrhihjz(4, 4),hibrhihjhk(4, 4, 4),gamtot(4)
    double precision hcpbrl,hcpbrm,hcpbrs,hcpbrc,hcpbrb,hcpbrt, &
       hcpbrbu,hcpbrcd,hcpbrts,hcpbrtd,hcpbrwh(4),gamtotch

    COMMON/CN2HDMHi_BRS/hibrt,hibrb,hibrl,hibrm, &
         hibrs,hibrc,hibrg,hibrga,hibrzga,hibrw,hibrhdhd, &
         hibrz,hibrhpwm,hibrchch,hibrhihjz,hibrhihjhk,gamtot
    COMMON/CN2HDMHc_BRS/hcpbrl,hcpbrm,hcpbrs,hcpbrc,hcpbrb,hcpbrt, &
       hcpbrbu,hcpbrcd,hcpbrts,hcpbrtd,hcpbrwh,gamtotch

! common block variables for top decay
    double precision GAT, GAB, GLT, GLB, GHT, GHB, GZAH, GZAL, &
        GHHH, GLLL, GHLL, GLHH, GHAA, GLAA, GLVV, GHVV, &
        GLPM, GHPM, B, A

    COMMON/COUP_HDEC/GAT, GAB, GLT, GLB, GHT, GHB, GZAH, GZAL, &
        GHHH, GLLL, GHLL, GLHH, GHAA, GLAA, GLVV, GHVV, &
        GLPM, GHPM, B, A

! Common Input blocks

    IHIGGS = 5
    IOELW = 0
    ISM4 = 0
    IGGELW = 0
    IFERMPHOB = 0
    I2HDM = 1
    ICN2HDM = 1
    IMODEL = 1
    TGBET = tbeta

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    VS = vevS
    TGBETCN2HDM = tbeta

    ami_cn2hdm(1) = mH1
    ami_cn2hdm(2) = mH2
    ami_cn2hdm(3) = mH3
    ami_cn2hdm(4) = mH4
    ami_cn2hdm(5) = mHD
    ami_cn2hdm(6) = mHc

    alam_cn2hdm(1) = L1
    alam_cn2hdm(2) = L2
    alam_cn2hdm(3) = L3
    alam_cn2hdm(4) = L4
    alam_cn2hdm(5) = RL5
    alam_cn2hdm(6) = L6
    alam_cn2hdm(7) = L7
    alam_cn2hdm(8) = L8
    alam_cn2hdm(9) = IL5

    c1 = cos(a1)
    c2 = cos(a2)
    c3 = cos(a3)
    c4 = cos(a4)
    c5 = cos(a5)
    c6 = cos(a6)
    s1 = sin(a1)
    s2 = sin(a2)
    s3 = sin(a3)
    s4 = sin(a4)
    s5 = sin(a5)
    s6 = sin(a6)

    R11 = cos(a1)*cos(a2)*cos(a6)
    R12 = cos(a2)*cos(a6)*sin(a1)
    R13 = cos(a6)*sin(a2)
    R14 = -sin(a6)

    R21= -s1*(c3*c4 + s3*s4*s5) + (c1*( -1*s2*(c4*s3 - c3*s4*s5) - c2*c5*s4*s6 ))
    R22 = c1*(c3*c4 + s3*s4*s5) + (s1*(-1*s2*(c4*s3-c3*s4*s5)-c2*c5*s4*s6))
    R23 = c2*(c4*s3 - c3*s4*s5) - c5*s2*s4*s6
    R24 = -(c5*c6*s4)

    R31 = c5*s1*s3 + c1*(-(c3*c5*s2) - c2*s5*s6)
    R32 = -(c1*c5*s3) + s1*(-(c3*c5*s2) - c2*s5*s6)
    R33 = c2*c3*c5 - s2*s5*s6
    R34 = -(c6*s5)

    R41 =  (c1*c2*c4*c5*s6) + (-s1*(c3*s4 - c4*s3*s5) -(c1*s2*(s3*s4 + c3*c4*s5)))
    R42 = (c1*(c3*s4 - c4*s3*s5) - s1*s2*(s3*s4 + c3*c4*s5) ) + (s1*c2*c4*c5*s6)
    R43= c2*(s3*s4 + c3*c4*s5) + c4*c5*s2*s6
    R44 = c4*c5*c6

    rij_cn2hdm(1,1) = R11
    rij_cn2hdm(1,2) = R12
    rij_cn2hdm(1,3) = R13
    rij_cn2hdm(1,4) = R14
    rij_cn2hdm(2,1) = R21
    rij_cn2hdm(2,2) = R22
    rij_cn2hdm(2,3) = R23
    rij_cn2hdm(2,4) = R24
    rij_cn2hdm(3,1) = R31
    rij_cn2hdm(3,2) = R32
    rij_cn2hdm(3,3) = R33
    rij_cn2hdm(3,4) = R34
    rij_cn2hdm(4,1) = R41
    rij_cn2hdm(4,2) = R42
    rij_cn2hdm(4,3) = R43
    rij_cn2hdm(4,4) = R44


    AMCH_CN2HDM = mHc
    AMCH = mHc
    AM12SQ = re_m12sq
    ITYPECN2HDM = type2HDM

    NNLO = 1
    IONSH = 0
    IONWZ = 0
    IPOLE = 0
    IOFSUSY = 1
    NFGG = 5
    IGOLD = 0
    INDIDEC = 0
    ICOUPELW = 0

    ! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)


! set the Hc couplings used in the top decay calculation
    if (ITYPECN2HDM .eq. 1) then
        gat = 1.D0/TGBETCN2HDM
        gab = -1.D0/TGBETCN2HDM
    elseif (ITYPECN2HDM .eq. 2) then
        gat = 1.D0/TGBETCN2HDM
        gab = TGBETCN2HDM
    elseif (ITYPECN2HDM .eq. 3) then
        gat = 1.D0/TGBETCN2HDM
        gab = -1.D0/TGBETCN2HDM
    elseif (ITYPECN2HDM .eq. 4) then
        gat = 1.D0/TGBETCN2HDM
        gab = TGBETCN2HDM
    endif


 !Run HDecay
    CALL HDEC_CN2HDM(tgbet)

    BR = (/gamtot(1), hibrb(1), hibrl(1), hibrm(1), hibrs(1), hibrc(1), &
           hibrt(1), hibrg(1), hibrga(1), hibrzga(1), hibrw(1), hibrz(1), &
           hibrhpwm(1), hibrchch(1), &
           gamtot(2), hibrb(2), hibrl(2), hibrm(2), hibrs(2), hibrc(2), &
           hibrt(2), hibrg(2), hibrga(2), hibrzga(2), hibrw(2), hibrz(2), &
           hibrhihjz(2, 1), hibrhpwm(2), hibrhihjhk(2, 1, 1), hibrchch(2), &
           gamtot(3), hibrb(3), hibrl(3), hibrm(3), hibrs(3), hibrc(3), &
           hibrt(3), hibrg(3), hibrga(3), hibrzga(3), hibrw(3), hibrz(3), &
           hibrhihjz(3, 1), hibrhihjz(3, 2), hibrhpwm(3), &
           hibrhihjhk(3, 1, 1), hibrhihjhk(3, 2, 2), hibrhihjhk(3, 1, 2), &
           hibrchch(3), &
           gamtotch, hcpbrb, hcpbrl, hcpbrm, hcpbrs, hcpbrc, hcpbrt, &
           hcpbrcd, hcpbrbu, hcpbrts, hcpbrtd, &
           hcpbrwh(1), hcpbrwh(2), hcpbrwh(3), hcpbrwh(4), &
           gamt1, gamt0/gamt1, (gamt1 - gamt0)/gamt1, &
           gamtot(4), hibrb(4), hibrl(4), hibrm(4), hibrs(4), hibrc(4), &
           hibrt(4), hibrg(4), hibrga(4), hibrzga(4), hibrw(4), hibrz(4), &
           hibrhpwm(4), hibrchch(4), &
           hibrhihjhk(4, 1, 1), hibrhihjhk(4, 2, 2), hibrhihjhk(4, 3, 3), &
           hibrhihjhk(4, 1, 2), hibrhihjhk(4, 1, 3), hibrhihjhk(4, 2, 3), &
           hibrhihjz(4, 1), hibrhihjz(4, 2), hibrhihjz(4, 3),&
	       hibrhdhd(1), hibrhdhd(2), hibrhdhd(3), hibrhdhd(4)/)



    return


end
