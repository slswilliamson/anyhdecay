subroutine hdecay_RxSM_broken(alpha, mH1, mH2, vs, &
                              smparams, &
                              BR) bind(C, name="hdecay_RxSM_broken")
    use, intrinsic :: iso_c_binding

    implicit none
    real(kind=c_double), intent(in), value :: alpha, mH1, mH2, vs
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) :: BR(26)

! common blocks from read_hdec
    integer IOELW
    integer IHIGGS, NNLO, IPOLE
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer IONSH, IONWZ, IOFSUSY
    integer NFGG
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision CPTP, CPBP, CPNUP, CPEP
    double precision AMS, AMC, AMB, AMT
    double precision AMSB
    double precision AMCB, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0
    integer ISINGLET, ICXSM
    double precision rescal(3)
    double precision am1cx, am2cx, am3cx
    double precision gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/CPSM4_HDEC/CPTP, CPBP, CPNUP, CPEP
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/MODCXSM/ISINGLET, ICXSM, rescal, am1cx, am2cx, am3cx
    COMMON/SELFCOUPSCX/gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333

! common blocks from WRITE_HDEC
    double precision SCXWDTH(3), SCXBRT(3), SCXBRB(3), &
        SCXBRL(3), SCXBRM(3), SCXBRC(3), SCXBRS(3), SCXBRG(3), &
        SCXBRGA(3), SCXBRZGA(3), SCXBRW(3), SCXBRZ(3)
    double precision SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2
    COMMON/WIDTHCX_HDEC/SCXBRB, SCXBRL, SCXBRM, SCXBRS, SCXBRC, SCXBRT, &
        SCXBRG, SCXBRGA, SCXBRZGA, SCXBRW, SCXBRZ, SCXWDTH
    COMMON/HHHDECCX_HDEC/SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2

! local variables hdecay stuff
    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber
! local variable singlet stuff
    double precision alphacx1, vscx, vcx, dc1, ds1, R1h, R1s, R2h, R2s, &
        alambda, am2s, alambdas, amass2, alambdahs, xnormval

    IOELW = 0
    IHIGGS = 0
    ISM4 = 0
    IFERMPHOB = 0
    IONWZ = 0
    IOFSUSY = 1
    NFGG = 5
    CPW = 1.
    CPZ = 1.
    CPTAU = 1.
    CPMU = 1.
    CPT = 1.
    CPB = 1.
    CPC = 1.
    CPS = 1.
    CPGAGA = 0.
    CPGG = 0.
    CPZGA = 0.
    CPTP = 0.
    CPBP = 0.
    CPNUP = 0.
    CPEP = 0.

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    ISINGLET = 1
    ICXSM = 1
    alphacx1 = alpha
    am1cx = mH1
    am2cx = mH2
    vscx = vs
    vcx = 1.D0/dsqrt(dsqrt(2.D0)*gf)

    dc1 = dcos(alphacx1)
    ds1 = dsin(alphacx1)

    R1h = dc1
    R1s = ds1
    R2h = -ds1
    R2s = dc1

    alambda = (2.D0*(am1cx**2 - am1cx**2*ds1**2 + am2cx**2* &
                     ds1**2))/vcx**2
    am2S = (-(am1cx**2*ds1*(dc1*vcx + ds1*vscx)) + am2cx**2* &
            (dc1*ds1*vcx + (-1.D0 + ds1**2)*vscx))/(2.D0*vscx)
    alambdas = (3.D0*(am2cx**2 + (am1cx**2 - am2cx**2)*ds1**2)) &
               /vscx**2
    amass2 = (-3.D0*alambda*vcx**4 + 12.D0*am2S*vscx**2 + &
              2.D0*alambdas*vscx**4)/(6.D0*vcx**2)
    alambdahs = -(6.D0*am2S + alambdas*vscx**2)/(3.D0*vcx**2)

    gg111 = (3.D0*alambda*R1h**3*vcx)/2.D0 + 3.D0*alambdahs* &
            R1h*R1s**2*vcx + 3.D0*alambdahs*R1h**2*R1s*vscx + &
            alambdas*R1s**3*vscx

    gg112 = 2.D0*alambdahs*R1h*R1s*(R2s*vcx + R2h*vscx) + &
            R1h**2*((3.D0*alambda*R2h*vcx)/2.D0 + alambdahs*R2s*vscx) &
            + R1s**2*(alambdahs*R2h*vcx + alambdas*R2s*vscx)

    gg122 = R1h*((3.D0*alambda*R2h**2*vcx)/2.D0 &
                 + alambdahs*R2s**2*vcx + 2.D0*alambdahs*R2h*R2s*vscx) &
            + R1s*(2.D0*alambdahs*R2h*R2s*vcx + alambdahs*R2h**2* &
                   vscx + alambdas*R2s**2*vscx)

    gg222 = (3.D0*alambda*R2h**3*vcx)/2.D0 + 3.D0*alambdahs*R2h* &
            R2s**2*vcx + 3.D0*alambdahs*R2h**2*R2s*vscx &
            + alambdas*R2s**3*vscx

    XNORMVAL = AMZ**2/VCX

    gg111 = gg111/XNORMVAL
    gg112 = gg112/XNORMVAL
    gg122 = gg122/XNORMVAL
    gg222 = gg222/XNORMVAL

    rescal(1) = dcos(ALPHACX1)
    rescal(2) = -dsin(ALPHACX1)

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

! run main code
    call HDEC_CXSM(1.)

! pass results
    BR = (/scxwdth(1), &
           scxbrb(1), scxbrl(1), scxbrm(1), scxbrs(1), scxbrc(1), scxbrt(1), &
           scxbrg(1), scxbrga(1), scxbrzga(1), scxbrw(1), scxbrz(1), &
           scxbrh1h2h2, &
           scxwdth(2), &
           scxbrb(2), scxbrl(2), scxbrm(2), scxbrs(2), scxbrc(2), scxbrt(2), &
           scxbrg(2), scxbrga(2), scxbrzga(2), scxbrw(2), scxbrz(2), &
           scxbrh2h1h1 &
           /)

    return
end

subroutine hdecay_RxSM_dark(mH1, mD, m2sq, &
                            smparams, &
                            BR) bind(C, name="hdecay_RxSM_dark")
    use, intrinsic :: iso_c_binding

    implicit none
    real(kind=c_double), intent(in), value :: mH1, mD, m2sq
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) :: BR(13)

! common blocks from read_hdec
    integer IOELW
    integer IHIGGS, NNLO, IPOLE
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer IONSH, IONWZ, IOFSUSY
    integer NFGG
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision CPTP, CPBP, CPNUP, CPEP
    double precision AMS, AMC, AMB, AMT
    double precision AMSB
    double precision AMCB, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0
    integer ISINGLET, ICXSM
    double precision rescal(3)
    double precision am1cx, am2cx, am3cx
    double precision gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/CPSM4_HDEC/CPTP, CPBP, CPNUP, CPEP
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/MODCXSM/ISINGLET, ICXSM, rescal, am1cx, am2cx, am3cx
    COMMON/SELFCOUPSCX/gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333

! common blocks from WRITE_HDEC
    double precision SCXWDTH(3), SCXBRT(3), SCXBRB(3), &
        SCXBRL(3), SCXBRM(3), SCXBRC(3), SCXBRS(3), SCXBRG(3), &
        SCXBRGA(3), SCXBRZGA(3), SCXBRW(3), SCXBRZ(3)
    double precision SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2
    COMMON/WIDTHCX_HDEC/SCXBRB, SCXBRL, SCXBRM, SCXBRS, SCXBRC, SCXBRT, &
        SCXBRG, SCXBRGA, SCXBRZGA, SCXBRW, SCXBRZ, SCXWDTH
    COMMON/HHHDECCX_HDEC/SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2

! local variables hdecay stuff
    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber
! local variable singlt stuff
    double precision vcx, amd, am2s, amass2, alambda, alambdahs, xnormval

    IOELW = 0
    IHIGGS = 0
    ISM4 = 0
    IFERMPHOB = 0
    IONWZ = 0
    IOFSUSY = 1
    NFGG = 5
    CPW = 1.
    CPZ = 1.
    CPTAU = 1.
    CPMU = 1.
    CPT = 1.
    CPB = 1.
    CPC = 1.
    CPS = 1.
    CPGAGA = 0.
    CPGG = 0.
    CPZGA = 0.
    CPTP = 0.
    CPBP = 0.
    CPNUP = 0.
    CPEP = 0.

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    ISINGLET = 1
    ICXSM = 2
    vcx = 1.D0/dsqrt(dsqrt(2.D0)*gf)

    am1cx = mH1
    amd = mD
    am2cx = mD
    am2s = m2sq

    amass2 = -am1cx**2
    alambda = (2.D0*am1cx**2)/vcx**2
    alambdahs = (-2.D0*(am2s - amd**2))/vcx**2

    gg111 = (3.D0*alambda*vcx)/2.D0
    gg112 = 0.D0
    gg122 = alambdahs*vcx
    gg222 = 0.D0

    XNORMVAL = AMZ**2/VCX

    gg111 = gg111/XNORMVAL
    gg112 = gg112/XNORMVAL
    gg122 = gg122/XNORMVAL
    gg222 = gg222/XNORMVAL

    rescal(1) = 1.D0

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

! run main code
    call HDEC_CXSM(1.)

! pass results
    BR = (/scxwdth(1), &
           scxbrb(1), scxbrl(1), scxbrm(1), scxbrs(1), scxbrc(1), scxbrt(1), &
           scxbrg(1), scxbrga(1), scxbrzga(1), scxbrw(1), scxbrz(1), &
           scxbrh1h2h2 &
           /)

    return
end

subroutine hdecay_CxSM_broken(alpha1, alpha2, alpha3, m1, m3, vs, &
                              smparams, &
                              BR) bind(c, name="hdecay_CxSM_broken")
    use, intrinsic :: iso_c_binding

    implicit none
    real(kind=c_double), intent(in), value :: alpha1, alpha2, alpha3, &
                                              m1, m3, vs
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) :: BR(45)

! common blocks from read_hdec
    integer IOELW
    integer IHIGGS, NNLO, IPOLE
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer IONSH, IONWZ, IOFSUSY
    integer NFGG
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision CPTP, CPBP, CPNUP, CPEP
    double precision AMS, AMC, AMB, AMT
    double precision AMSB
    double precision AMCB, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0
    integer ISINGLET, ICXSM
    double precision rescal(3)
    double precision am1cx, am2cx, am3cx
    double precision gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/CPSM4_HDEC/CPTP, CPBP, CPNUP, CPEP
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/MODCXSM/ISINGLET, ICXSM, rescal, am1cx, am2cx, am3cx
    COMMON/SELFCOUPSCX/gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333

! common blocks from WRITE_HDEC
    double precision SCXWDTH(3), SCXBRT(3), SCXBRB(3), &
        SCXBRL(3), SCXBRM(3), SCXBRC(3), SCXBRS(3), SCXBRG(3), &
        SCXBRGA(3), SCXBRZGA(3), SCXBRW(3), SCXBRZ(3)
    double precision SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2
    COMMON/WIDTHCX_HDEC/SCXBRB, SCXBRL, SCXBRM, SCXBRS, SCXBRC, SCXBRT, &
        SCXBRG, SCXBRGA, SCXBRZGA, SCXBRW, SCXBRZ, SCXWDTH
    COMMON/HHHDECCX_HDEC/SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2

! local variables hdecay stuff
    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber
! local variable singlt stuff
    double precision vcx, alphacx1, alphacx2, alphacx3, vscx, &
        dc1, ds1, dc2, ds2, dc3, ds3, &
        r1h, r1s, r1a, r2h, r2s, r2a, r3h, r3s, r3a, &
        va, alambda, b2, d2, a1, amass2, delta2, b1, &
        xnormval

    IOELW = 0
    IHIGGS = 0
    ISM4 = 0
    IFERMPHOB = 0
    IONWZ = 0
    IOFSUSY = 1
    NFGG = 5
    CPW = 1.
    CPZ = 1.
    CPTAU = 1.
    CPMU = 1.
    CPT = 1.
    CPB = 1.
    CPC = 1.
    CPS = 1.
    CPGAGA = 0.
    CPGG = 0.
    CPZGA = 0.
    CPTP = 0.
    CPBP = 0.
    CPNUP = 0.
    CPEP = 0.

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    ISINGLET = 1
    ICXSM = 3
    vcx = 1.D0/dsqrt(dsqrt(2.D0)*gf)

    ALPHACX1 = alpha1
    ALPHACX2 = alpha2
    ALPHACX3 = alpha3
    AM1CX = m1
    AM3CX = m3
    VSCX = vs

    dc1 = dcos(alphacx1)
    ds1 = dsin(alphacx1)
    dc2 = dcos(alphacx2)
    ds2 = dsin(alphacx2)
    dc3 = dcos(alphacx3)
    ds3 = dsin(alphacx3)

! Higg mixing matrix elements
    R1h = dc1*dc2
    R1s = dc2*ds1
    R1a = ds2
    R2h = -(dc3*ds1) - dc1*ds2*ds3
    R2s = dc1*dc3 - ds1*ds2*ds3
    R2a = dc2*ds3
    R3h = -(dc1*dc3*ds2) + ds1*ds3
    R3s = -(dc3*ds1*ds2) - dc1*ds3
    R3a = dc2*dc3

! mass AM2CX
    AM2CX = dsqrt(-((am1cx**2*am3cx**2*R2h*R2s) &
                    /(am3cx**2*R1h*R1s + am1cx**2*R3h*R3s)))

! some definitions
    va = ((dc3*ds1*(am3cx**2 + (am1cx**2 - am3cx**2)*ds2**2) &
           + dc1*am1cx**2*ds2*ds3)*vscx)/(dc2*(am1cx**2 &
                                               - am3cx**2)*ds1*(dc3*ds1*ds2 + dc1*ds3))

    alambda = (-2.D0*am3cx**2*(dc1*ds1*(-1.D0 + 2.D0*ds3**2)* &
                               ((1.D0 - 2.D0*ds2**2)*va*vscx + dc2*ds1*ds2*(va - vscx)*(va + vscx)) &
                               + dc3*ds3*(ds2*(ds1**2 + ds2**2 - 3.D0*ds1**2*ds2**2)*va*vscx + &
                                          dc2*ds1*(-1.D0 + ds1**2*(1.D0 + ds2**2))*(va - vscx)*(va + vscx)))) &
              /((dc3*ds1*ds2 + dc1*ds3)*vcx**2*(dc2*ds1*va - ds2*vscx)* &
                (dc1*dc3*va - ds3*(ds1*ds2*va + dc2*vscx)))

    b2 = (am3cx**2*vscx*((dc3*ds2*(-ds1**2 + (-1.D0 + 3.D0*ds1**2)*ds2**2)*ds3 &
                          + dc1*ds1*(-1.D0 + 2.D0*ds2**2)*(-1.D0 + 2.D0*ds3**2))*va**2 &
                         - 2.D0*dc3*ds2*(-1.D0 + ds2**2)*ds3*vscx**2 &
                         + dc2*va*((ds1*(-1.D0 + ds1**2)*ds2 &
                                    + dc1*dc3*(ds1**2 + (-1.D0 + ds1**2)*ds2**2)*ds3 &
                                    - 2.D0*ds1*(-1.D0 + ds1**2)*ds2*ds3**2)*vcx &
                                   + (dc3*ds1*(-2.D0 + ds1**2 + (3.D0 + ds1**2)*ds2**2)*ds3 &
                                      + dc1*(1.D0 + ds1**2)*ds2*(-1.D0 + 2.D0*ds3**2))*vscx))) &
         /((dc3*ds1*ds2 + dc1*ds3)*va*(dc2*ds1*va - ds2*vscx)* &
           (dc1*dc3*va - ds3*(ds1*ds2*va + dc2*vscx)))

    d2 = (-2.D0*am3cx**2*(dc2*ds1*(dc3*(-1.D0 + ds1**2*(1.D0 + ds2**2))*ds3 &
                                   + dc1*ds1*ds2*(-1.D0 + 2.D0*ds3**2))*va &
                          - dc3*ds2*(-1.D0 + ds2**2)*ds3*vscx)) &
         /((dc3*ds1*ds2 + dc1*ds3)*va*(dc2*ds1*va - ds2*vscx) &
           *(dc1*dc3*va - ds3*(ds1*ds2*va + dc2*vscx)))

    a1 = -((am3cx**2*vscx*(dc3*ds1*ds2*va + dc1*ds3*va + dc2*dc3*vscx)) &
           /(dsqrt(2.D0)*(dc3*ds1*ds2 + dc1*ds3)*va))

    amass2 = (-(alambda*vcx**4*vscx) &
              + (va**2 + vscx**2)*(2.D0*b2*vscx + d2*vscx*(va**2 + vscx**2) + 2.D0*a1*dsqrt(2.D0))) &
             /(2.D0*vcx**2*vscx)

    delta2 = -((2.D0*b2*vscx + d2*vscx*(va**2 + vscx**2) + 2.D0*a1*dsqrt(2.D0)) &
               /(vcx**2*vscx))

    b1 = -((a1*dsqrt(2.D0))/vscx)

! trilinear Higgs self-couplings
    gg111 = (3.D0*(alambda*R1h**3*vcx &
                   + delta2*R1h*(R1a**2 + R1s**2)*vcx + delta2*R1h**2*(R1a*va + R1s*vscx) &
                   + d2*(R1a**2 + R1s**2)*(R1a*va + R1s*vscx))) &
            /2.D0

    gg112 = (2.D0*d2*R1a*R1s*(R2s*va + R2a*vscx) &
             + 2.D0*delta2*R1h*(R1a*R2a*vcx + R1s*R2s*vcx + R1a*R2h*va + R1s*R2h*vscx) &
             + R1a**2*(delta2*R2h*vcx + 3.D0*d2*R2a*va + d2*R2s*vscx) &
             + R1s**2*(delta2*R2h*vcx + d2*R2a*va + 3*d2*R2s*vscx) &
             + R1h**2*(3.D0*alambda*R2h*vcx + delta2*(R2a*va + R2s*vscx))) &
            /2.D0

    gg113 = (2.D0*d2*R1a*R1s*(R3s*va + R3a*vscx) &
             + 2.D0*delta2*R1h*(R1a*R3a*vcx + R1s*R3s*vcx + R1a*R3h*va + R1s*R3h*vscx) &
             + R1a**2*(delta2*R3h*vcx + 3.D0*d2*R3a*va + d2*R3s*vscx) &
             + R1s**2*(delta2*R3h*vcx + d2*R3a*va + 3.D0*d2*R3s*vscx) &
             + R1h**2*(3*alambda*R3h*vcx + delta2*(R3a*va + R3s*vscx))) &
            /2.D0

    gg122 = (R1a*(2.D0*delta2*R2a*R2h*vcx + 3.D0*d2*R2a**2*va + &
                  delta2*R2h**2*va + d2*R2s**2*va + 2.D0*d2*R2a*R2s*vscx) &
             + R1h*(3.D0*alambda*R2h**2*vcx + delta2*(R2a**2 + R2s**2)*vcx &
                    + 2.D0*delta2*R2h*(R2a*va + R2s*vscx)) &
             + R1s*(2.D0*delta2*R2h*R2s*vcx + delta2*R2h**2*vscx + &
                    d2*(2.D0*R2a*R2s*va + R2a**2*vscx + 3.D0*R2s**2*vscx))) &
            /2.D0

    gg123 = (R1a*(delta2*R2h*(R3a*vcx + R3h*va) + d2*R2s*(R3s*va + R3a*vscx) &
                  + R2a*(delta2*R3h*vcx + 3.D0*d2*R3a*va + d2*R3s*vscx)) &
             + R1h*(delta2*(R2a*R3a*vcx + R2s*R3s*vcx + R2a*R3h*va + R2s*R3h*vscx) &
                    + R2h*(3.D0*alambda*R3h*vcx + delta2*(R3a*va + R3s*vscx))) &
             + R1s*(d2*R2a*(R3s*va + R3a*vscx) + delta2*R2h*(R3s*vcx + R3h*vscx) + &
                    R2s*(delta2*R3h*vcx + d2*(R3a*va + 3.D0*R3s*vscx)))) &
            /2.D0

    gg133 = (R1a*(2.D0*delta2*R3a*R3h*vcx + 3.D0*d2*R3a**2*va + &
                  delta2*R3h**2*va + d2*R3s**2*va + 2.D0*d2*R3a*R3s*vscx) &
             + R1h*(3.D0*alambda*R3h**2*vcx + delta2*(R3a**2 + R3s**2)*vcx &
                    + 2.D0*delta2*R3h*(R3a*va + R3s*vscx)) &
             + R1s*(2.D0*delta2*R3h*R3s*vcx + delta2*R3h**2*vscx &
                    + d2*(2.D0*R3a*R3s*va + R3a**2*vscx + 3.D0*R3s**2*vscx))) &
            /2.D0

    gg222 = (3.D0*(alambda*R2h**3*vcx + delta2*R2h*(R2a**2 + R2s**2)*vcx &
                   + delta2*R2h**2*(R2a*va + R2s*vscx) + d2*(R2a**2 + R2s**2)*(R2a*va + R2s*vscx))) &
            /2.D0

    gg223 = (2.D0*d2*R2a*R2s*(R3s*va + R3a*vscx) &
             + 2.D0*delta2*R2h*(R2a*R3a*vcx + R2s*R3s*vcx + R2a*R3h*va + R2s*R3h*vscx) &
             + R2a**2*(delta2*R3h*vcx + 3.D0*d2*R3a*va + d2*R3s*vscx) &
             + R2s**2*(delta2*R3h*vcx + d2*R3a*va + 3*d2*R3s*vscx) &
             + R2h**2*(3.D0*alambda*R3h*vcx + delta2*(R3a*va + R3s*vscx))) &
            /2.D0

    gg233 = (R2a*(2.D0*delta2*R3a*R3h*vcx + 3.D0*d2*R3a**2*va &
                  + delta2*R3h**2*va + d2*R3s**2*va + 2.D0*d2*R3a*R3s*vscx) &
             + R2h*(3.D0*alambda*R3h**2*vcx + delta2*(R3a**2 + R3s**2)*vcx &
                    + 2.D0*delta2*R3h*(R3a*va + R3s*vscx)) &
             + R2s*(2.D0*delta2*R3h*R3s*vcx + delta2*R3h**2*vscx &
                    + d2*(2.D0*R3a*R3s*va + R3a**2*vscx + 3.D0*R3s**2*vscx))) &
            /2.D0

    gg333 = (3.D0*(alambda*R3h**3*vcx + delta2*R3h*(R3a**2 + R3s**2)*vcx &
                   + delta2*R3h**2*(R3a*va + R3s*vscx) &
                   + d2*(R3a**2 + R3s**2)*(R3a*va + R3s*vscx))) &
            /2.D0

    XNORMVAL = AMZ**2/VCX

    gg111 = gg111/XNORMVAL
    gg112 = gg112/XNORMVAL
    gg113 = gg113/XNORMVAL
    gg122 = gg122/XNORMVAL
    gg123 = gg123/XNORMVAL
    gg133 = gg133/XNORMVAL
    gg222 = gg222/XNORMVAL
    gg223 = gg223/XNORMVAL
    gg233 = gg233/XNORMVAL
    gg333 = gg333/XNORMVAL

    rescal(1) = dcos(ALPHACX1)*dcos(ALPHACX2)
    rescal(2) = -(dcos(ALPHACX1)*dsin(ALPHACX2)*dsin(ALPHACX3) + &
                  dsin(ALPHACX1)*dcos(ALPHACX3))
    rescal(3) = -dcos(ALPHACX1)*dsin(ALPHACX2)*dcos(ALPHACX3) &
                + dsin(ALPHACX1)*dsin(ALPHACX3)

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

! run main code
    call HDEC_CXSM(1.)

! pass results
    BR = (/scxwdth(1), &
           scxbrb(1), scxbrl(1), scxbrm(1), scxbrs(1), scxbrc(1), scxbrt(1), &
           scxbrg(1), scxbrga(1), scxbrzga(1), scxbrw(1), scxbrz(1), &
           scxbrh1h2h2, scxbrh1h2h3, scxbrh1h3h3, &
           scxwdth(2), &
           scxbrb(2), scxbrl(2), scxbrm(2), scxbrs(2), scxbrc(2), scxbrt(2), &
           scxbrg(2), scxbrga(2), scxbrzga(2), scxbrw(2), scxbrz(2), &
           scxbrh2h1h1, scxbrh2h1h3, scxbrh2h3h3, &
           scxwdth(3), &
           scxbrb(3), scxbrl(3), scxbrm(3), scxbrs(3), scxbrc(3), scxbrt(3), &
           scxbrg(3), scxbrga(3), scxbrzga(3), scxbrw(3), scxbrz(3), &
           scxbrh3h1h1, scxbrh3h1h2, scxbrh3h2h2 &
           /)

    return
end

subroutine hdecay_CxSM_dark(alpha, mH1, mH2, mD, vs, a1, &
                            smparams, &
                            BR) bind(C, name="hdecay_CxSM_dark")
    use, intrinsic :: iso_c_binding
    implicit none
    real(kind=c_double), intent(in), value :: alpha, mH1, mH2, mD, vs, a1
    real(kind=c_double), intent(in) :: smparams(22)
    real(kind=c_double), intent(out) :: BR(30)

! common blocks from read_hdec
    integer IOELW
    integer IHIGGS, NNLO, IPOLE
    double precision AMTP, AMBP, AMNUP, AMEP
    integer ISM4, IGGELW
    integer IFERMPHOB
    integer IMODEL
    integer IONSH, IONWZ, IOFSUSY
    integer NFGG
    double precision CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA
    integer ICOUPELW
    double precision CPTP, CPBP, CPNUP, CPEP
    double precision AMS, AMC, AMB, AMT
    double precision AMSB
    double precision AMCB, AMBB
    double precision GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    double precision VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    double precision GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    double precision XLAMBDA, AMC0, AMB0, AMT0
    integer N0
    integer ISINGLET, ICXSM
    double precision rescal(3)
    double precision am1cx, am2cx, am3cx
    double precision gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333
    COMMON/OMIT_ELW_HDEC/IOELW
    COMMON/FLAG_HDEC/IHIGGS, NNLO, IPOLE
    COMMON/SM4_HDEC/AMTP, AMBP, AMNUP, AMEP, ISM4, IGGELW
    COMMON/FERMIOPHOBIC_HDEC/IFERMPHOB
    COMMON/MODEL_HDEC/IMODEL
    COMMON/ONSHELL_HDEC/IONSH, IONWZ, IOFSUSY
    COMMON/OLDFASH_HDEC/NFGG
    COMMON/CPSM_HDEC/CPW, CPZ, CPTAU, CPMU, CPT, CPB, CPC, CPS, &
        CPGAGA, CPGG, CPZGA, ICOUPELW
    COMMON/CPSM4_HDEC/CPTP, CPBP, CPNUP, CPEP
    COMMON/MASSES_HDEC/AMS, AMC, AMB, AMT
    COMMON/STRANGE_HDEC/AMSB
    COMMON/MSBAR_HDEC/AMCB, AMBB
    COMMON/PARAM_HDEC/GF, ALPH, AMTAU, AMMUON, AMZ, AMW
    COMMON/CKMPAR_HDEC/VTB, VTS, VTD, VCB, VCS, VCD, VUB, VUS, VUD
    COMMON/WZWDTH_HDEC/GAMC0, GAMT0, GAMT1, GAMW, GAMZ
    COMMON/ALS_HDEC/XLAMBDA, AMC0, AMB0, AMT0, N0
    COMMON/MODCXSM/ISINGLET, ICXSM, rescal, am1cx, am2cx, am3cx
    COMMON/SELFCOUPSCX/gg111, gg112, gg113, gg122, gg123, gg133, gg222, &
        gg223, gg233, gg333

! common blocks from WRITE_HDEC
    double precision SCXWDTH(3), SCXBRT(3), SCXBRB(3), &
        SCXBRL(3), SCXBRM(3), SCXBRC(3), SCXBRS(3), SCXBRG(3), &
        SCXBRGA(3), SCXBRZGA(3), SCXBRW(3), SCXBRZ(3)
    double precision SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2
    COMMON/WIDTHCX_HDEC/SCXBRB, SCXBRL, SCXBRM, SCXBRS, SCXBRC, SCXBRT, &
        SCXBRG, SCXBRGA, SCXBRZGA, SCXBRW, SCXBRZ, SCXWDTH
    COMMON/HHHDECCX_HDEC/SCXBRH1H2H2, SCXBRH1H3H3, SCXBRH1H2H3, &
        SCXBRH2H1H1, SCXBRH2H3H3, SCXBRH2H1H3, SCXBRH3H1H1, SCXBRH3H2H2, &
        SCXBRH3H1H2

! local variables, hdecay stuff
    double precision alsmz, acc, del, ambl, ambu, fmb, xmb
    double precision runm_hdec, xitla_hdec
    integer nloop, nber
! local variables, singlet stuff
    double precision vcx, xnormval
    double precision alphacx1, alphacx2, alphacx3, vscx, a1cx, &
        dc1, ds1, amass2, delta2, alambda, b2, d2, b1

    IOELW = 0
    IHIGGS = 0
    ISM4 = 0
    IFERMPHOB = 0
    IONWZ = 0
    IOFSUSY = 1
    NFGG = 5
    CPW = 1.
    CPZ = 1.
    CPTAU = 1.
    CPMU = 1.
    CPT = 1.
    CPB = 1.
    CPC = 1.
    CPS = 1.
    CPGAGA = 0.
    CPGG = 0.
    CPZGA = 0.
    CPTP = 0.
    CPBP = 0.
    CPNUP = 0.
    CPEP = 0.

    ALSMZ = smparams(1)
    AMS = smparams(2)
    AMC = smparams(3)
    AMB = smparams(4)
    AMT = smparams(5)
    AMTAU = smparams(6)
    AMMUON = smparams(7)
    ALPH = smparams(8)
    GF = smparams(9)
    GAMW = smparams(10)
    GAMZ = smparams(11)
    AMZ = smparams(12)
    AMW = smparams(13)
    VTB = smparams(14)
    VTS = smparams(15)
    VTD = smparams(16)
    VCB = smparams(17)
    VCS = smparams(18)
    VCD = smparams(19)
    VUB = smparams(20)
    VUS = smparams(21)
    VUD = smparams(22)

    ISINGLET = 1
    ICXSM = 4
    vcx = 1.D0/dsqrt(dsqrt(2.D0)*gf)

    ALPHACX1 = alpha
    ALPHACX2 = 0.D0
    ALPHACX3 = 0.D0
    AM1CX = mH1
    AM2CX = mH2
    AM3CX = mD
    VSCX = vs
    A1CX = a1

    dc1 = dcos(alphacx1)
    ds1 = dsin(alphacx1)

! some definitions
!            amass2=(-(am2cx**2**2*ds1**2*vcx) + am1cx**2*(-1.D0
!     .           + ds1**2)*vcx + dc1*(-am1cx**2 + am2cx**2)*ds1*vscx)
!     .           /vcx

    amass2 = (-(am2cx**2*ds1**2*vcx) + am1cx**2*(-1.D0 + ds1**2)*vcx &
              + dc1*(-am1cx**2 + am2cx**2)*ds1*vscx)/vcx

    delta2 = (2.D0*dc1*(am1cx**2 - am2cx**2)*ds1)/(vcx*vscx)

    alambda = (2.D0*(am1cx**2 - am1cx**2*ds1**2 + am2cx**2*ds1**2))/vcx**2

    b2 = (dc1*(-am1cx**2 + am2cx**2)*ds1*vcx &
          + (am3cx**2 - am2cx**2 + (-am1cx**2 + am2cx**2)*ds1**2)*vscx &
          - 2.D0*a1cx*dsqrt(2.D0))/vscx

    d2 = (2.D0*((am2cx**2 + (am1cx**2 - am2cx**2)*ds1**2)*vscx &
                + a1cx*dsqrt(2.D0)))/vscx**3

    b1 = -((am3cx**2*vscx + a1cx*dsqrt(2.D0))/vscx)

! trilinear Higgs self-couplings
    gg111 = (3.D0*(dc1**3*alambda*vcx + dc1*delta2*ds1**2*vcx + &
                   dc1**2*delta2*ds1*vscx + d2*ds1**3*vscx))/2.D0

    gg112 = ((dc1**2*(2.D0*delta2 - 3.D0*alambda)*ds1*vcx) &
             - delta2*ds1**3*vcx + dc1**3*delta2*vscx + dc1* &
             (3.D0*d2 - 2.D0*delta2)*ds1**2*vscx)/2.D0

    gg113 = 0.D0

    gg122 = (dc1**3*delta2*vcx + dc1*(-2.D0*delta2 + 3.D0*alambda)* &
             ds1**2*vcx + dc1**2*(3.D0*d2 - 2.D0*delta2)*ds1*vscx &
             + delta2*ds1**3*vscx)/2.D0

    gg123 = 0.D0

    gg133 = (dc1*delta2*vcx + d2*ds1*vscx)/2.D0

    gg222 = (3.D0*(-(dc1**2*delta2*ds1*vcx) - alambda*ds1**3*vcx &
                   + dc1**3*d2*vscx + dc1*delta2*ds1**2*vscx))/2.D0

    gg223 = 0.D0

    gg233 = (-(delta2*ds1*vcx) + dc1*d2*vscx)/2.D0

    gg333 = 0.D0

    XNORMVAL = AMZ**2/VCX

    gg111 = gg111/XNORMVAL
    gg112 = gg112/XNORMVAL
    gg113 = gg113/XNORMVAL
    gg122 = gg122/XNORMVAL
    gg123 = gg123/XNORMVAL
    gg133 = gg133/XNORMVAL
    gg222 = gg222/XNORMVAL
    gg223 = gg223/XNORMVAL
    gg233 = gg233/XNORMVAL
    gg333 = gg333/XNORMVAL

    rescal(1) = dcos(ALPHACX1)
    rescal(2) = -dsin(ALPHACX1)
    rescal(3) = 0.D0

! **********************************************************************
! ***************   initialize alpha_s and polylogs    *****************
    ALPH = 1.D0/ALPH
    AMSB = AMS
    AMCB = AMC
    AMBB = AMB

    AMC0 = AMC
    AMB0 = AMB
    AMT0 = AMT
    ACC = 1.D-10
    NLOOP = 3
    XLAMBDA = XITLA_HDEC(NLOOP, ALSMZ, ACC)
    N0 = 5
    CALL ALSINI_HDEC(ACC)
!--DECOUPLING THE TOP QUARK FROM ALPHAS
    AMT0 = 3.D8

    del = 1.d-10
    ambl = ambb
    ambu = 2*ambb
    fmb = (ambl + ambu)/2
    acc = 1.d-10
    nloop = 3
12  amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    amc0 = amc
    n0 = 5
    call alsini_hdec(acc)
    xmb = runm_hdec(ambb, 5)
    if (abs(xmb - ambb) .lt. del) then
        ambl = fmb
        ambu = fmb
    elseif (xmb .gt. ambb) then
        ambu = fmb
    else
        ambl = fmb
    endif
    fmb = (ambl + ambu)/2
    if (dabs(xmb/ambb - 1) .gt. del) goto 12
    amb = fmb
    amc = amb - 3.41d0
    amb0 = amb
    n0 = 5
    call alsini_hdec(acc)
!--INITIALIZE COEFFICIENTS FOR POLYLOGARITHMS
    NBER = 18
    CALL BERNINI_HDEC(NBER)

! run main code
    call HDEC_CXSM(1.)

! pass results
    BR = (/scxwdth(1), &
           scxbrb(1), scxbrl(1), scxbrm(1), scxbrs(1), scxbrc(1), scxbrt(1), &
           scxbrg(1), scxbrga(1), scxbrzga(1), scxbrw(1), scxbrz(1), &
           scxbrh1h2h2, scxbrh1h2h3, scxbrh1h3h3, &
           scxwdth(2), &
           scxbrb(2), scxbrl(2), scxbrm(2), scxbrs(2), scxbrc(2), scxbrt(2), &
           scxbrg(2), scxbrga(2), scxbrzga(2), scxbrw(2), scxbrz(2), &
           scxbrh2h1h1, scxbrh2h1h3, scxbrh2h3h3 &
           /)

    return
end
